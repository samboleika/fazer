(function (lib, img, cjs, ss) {

var p; // shortcut to reference prototypes
lib.webFontTxtFilters = {}; 

// library properties:
lib.properties = {
	width: 500,
	height: 400,
	fps: 40,
	color: "#CCCCCC",
	webfonts: {},
	manifest: [
		{src:"img/logo_testo.png", id:"logo_testo"},
		{src:"img/samolet.png", id:"samolet"},
		{src:"img/testo.png", id:"testo"}
	]
};



lib.webfontAvailable = function(family) { 
	lib.properties.webfonts[family] = true;
	var txtFilters = lib.webFontTxtFilters && lib.webFontTxtFilters[family] || [];
	for(var f = 0; f < txtFilters.length; ++f) {
		txtFilters[f].updateCache();
	}
};
// symbols:



(lib.logo_testo = function() {
	this.initialize(img.logo_testo);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,147,145);


(lib.samolet = function() {
	this.initialize(img.samolet);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,80,63);


(lib.testo = function() {
	this.initialize(img.testo);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,428,392);


(lib.Символ12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.instance = new lib.logo_testo();
	this.instance.setTransform(-73.5,-72.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-73.5,-72.5,147,145);


(lib.Символ11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.instance = new lib.samolet();
	this.instance.setTransform(-40,-31.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-40,-31.5,80,63);


(lib.Символ10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AozHoQgGgEgJgNQgKgNgGgTIgHgYIgDgIIgDgKIgEgSIgCgOQgCgFAAgGIgBgQQgBgKAKgFQAFgDAHABQAGAAAGAEQAGAEABAFIABAMIAAAWQAAANACAKIAEARIADAMIADAJIAEAKIADALIAFARIACAOQAAAFgEADIgDACQgDAAgEgDgApzDTIgDgQIgCgSIgBgqIABgRIABgTQAAgFACgLIADgRQADgRAIgGQAEgEAHAAQAHAAAGAFQAGAFABAGQABAGgDAJQgDAJgBAQIgCAUIgDAeIgDAgQgBATgDANIgEAKQgEAEgGAAQgHgBgEgLgAJskrQgHgCgKgGIgQgMIgngcIgRgKQgLgGgNgDQgRgFgEgDQgFgDgDgFQgDgGACgFQADgIALgDQAIgDAIAAIAAAAQAaACAeAUQANAKAgAgQAKAKAGAJIAJAQQADAHgEACIgFABIgHgBgAFTmtQgngOgYABQgXAAgXgGQgXgFAMgOQAMgOAJgEQAHgDAQAAQAiAAAnAMQAwASAEALQADALgEAIQgEAGgMAAQgMAAgUgHgAg/mwQgEgCgEgEQgIgKAIgJQAFgGAJgDQAOgGAPgEQAbgGBmgIIANAAIABAAQApADAAALQAAALgpADIgqAFQgmAEgeAIIgqALIgQAFIgKgDg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-63.4,-49.1,127,98.2);


(lib.Символ13 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Символ 11
	this.instance = new lib.Символ11();
	this.instance.setTransform(-45.1,-46.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(54).to({scaleX:1,scaleY:1,rotation:19.1,guide:{path:[-45,-46.1,-43.6,-47.5,-42.1,-48.9,-34.2,-56.4,-25.1,-61.4]}},29,cjs.Ease.get(-1)).to({scaleX:1,scaleY:1,rotation:119.1,guide:{path:[-25.1,-61.3,-8,-70.6,13.1,-70.6,45.3,-70.6,68.2,-48.9,80.3,-37.4,86,-23.5]}},47).to({rotation:247.8,guide:{path:[86,-23.3,91.1,-10.9,91.1,3.6,91.1,34.1,68.2,55.8,45.3,77.5,13.1,77.5,-4.3,77.5,-18.9,71.3]}},50).to({scaleX:1,scaleY:1,rotation:323.4,guide:{path:[-19.2,71.2,-31.7,65.8,-42.2,55.8,-65.1,34,-65.1,3.5,-65.1,-0.8,-64.6,-4.9]}},35).to({scaleX:1,scaleY:1,rotation:360,guide:{path:[-64.6,-5.1,-62,-28.3,-45,-46.1]}},45,cjs.Ease.get(1)).wait(120));

	// Символ 10
	this.instance_1 = new lib.Символ10();
	this.instance_1.setTransform(-0.3,-20.6);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(54).to({alpha:0},14).to({_off:true},1).wait(177).to({_off:false},0).to({alpha:1},14).wait(120));

	// Символ 12
	this.instance_2 = new lib.Символ12();
	this.instance_2.setTransform(12,5.6);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(380));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-85.1,-77.7,170.6,155.8);


(lib.Символ48 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.instance = new lib.Символ13();
	this.instance.setTransform(359.6,258.6);

	this.instance_1 = new lib.testo();

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,445,392);


// stage content:
(lib.zovi_gostei = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		var r = this;
		var w = stage1.canvas.width;
		var h = stage1.canvas.height;

		if(w>h){
			
			r.clip.scaleX = h/400;
			r.clip.scaleY = h/400;
			
				if(r.clip.scaleY>=1){
					r.clip.scaleX = 1;
					r.clip.scaleY = 1;			
				}
			
		
		}else{
			
			r.clip.scaleX = w/500;
			r.clip.scaleY = w/500;	
			
				if(r.clip.scaleX>=1){
					r.clip.scaleX = 1;
					r.clip.scaleY = 1;			
				}	
			
		}
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// Слой 5
	this.clip = new lib.Символ48();

	this.timeline.addTween(cjs.Tween.get(this.clip).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(250,200,445,392);

})(lib1 = lib1||{}, images1 = images1||{}, createjs = createjs||{}, ss = ss||{});
var lib1, images1, createjs, ss;