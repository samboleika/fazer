(function (lib, img, cjs, ss) {

var p; // shortcut to reference prototypes
lib.webFontTxtFilters = {}; 

// library properties:
lib.properties = {
	width: 698,
	height: 430,
	fps: 40,
	color: "#CCCCCC",
	webfonts: {},
	manifest: [
		{src:"img/girl.png", id:"girl"}
	]
};



lib.webfontAvailable = function(family) { 
	lib.properties.webfonts[family] = true;
	var txtFilters = lib.webFontTxtFilters && lib.webFontTxtFilters[family] || [];
	for(var f = 0; f < txtFilters.length; ++f) {
		txtFilters[f].updateCache();
	}
};
// symbols:



(lib.girl = function() {
	this.initialize(img.girl);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,676,425);


(lib.Символ9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("ABfBCQgPgCgGgNQgEgKAGgUQAGgVgBgIIhLgDQgYgBgNgCIgbgFQgQgCgMACIgEATQgCAKgEAEQgFAGgIAAQgJAAgFgGQgEgHAAgMIAAgaQAAgSAGgIQAHgHAUgBIAXAAQALgBAQACIAaAFIAWABIBAAEQASABAJACQAQAEAIAKQALAOgCAaIgFAjQgDANgCADQgIAMgNAAIgCAAg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-12.9,-6.6,25.8,13.3);


(lib.Символ7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.instance = new lib.girl();
	this.instance.setTransform(-338,-212.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-338,-212,676,425);


(lib.Символ5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AACBIQgIgHgBgNIgCgcIgCgKIgPg9IAAgFQANgoAeAlQAEAHAEAUQADAUgBAVIgFAoQgCARgGADIgFABQgDAAgEgCg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-2.7,-7.4,5.5,14.9);


(lib.Символ4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("ABTDFIgSg4QgLglgCgOQgCgOgxhQQguhTgkgzQglgzABgGQABgGALACIAOAAQAeAXAoAzQAlAzAlA1QAkAzAQAuQAPAtgCAVQgDAUgEAOQgEAOgEAFQgFAEgHACIAAAAQgCAAgGgEg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-11.8,-20.1,23.7,40.3);


(lib.Символ3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AEUDgQgKgCgQgTQgRgTgOgUIgggwQgTgbgwgwQgighg6gvQglgigUgPQgigagegQQhPgphFAKIgeACQgSgCgHgMQgGgJAFgNQAFgLALgHQAPgJAfABIABgCQBdgBBRAuQAgASA4AuQBKA8AnAmQA8A3AmA3QAaAnAdA6QAIAOgDAIQgDAGgHADQgEACgEAAIgFAAg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-30,-22.4,60.1,44.9);


(lib.Символ2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AhiCoQh5g1gwhOIgFgIIgEgGQgCgFAAgIIAAgMIAAgmIgPhJQgQgkgFgGIACgDQAFgKAMgLQAcgXAkgMQAjgMAlABIAAAAQA1gCA2AiQAoAaAnAzQAdAnANAMQAQAPApAbQAoAXAZAMQAmASAhAEQAcAEAJAEQAJAFAEAJQAFAJgEAIQgDAIgJADQAAAJgDAOQgGAhgKASQgLASgcAXQgXATgSAFQgNAEgaAAQiAgBiFg6gAisixQg5ADg1AkQAMAdAGAhQAGAigGAdIAmAqQAmAqBoAyQBnAyBxAHQA6AEAbgXQATgRAHggIADgTIADgKQg2gOhCgmQhAgigjgkQgJgJgjgtQgcgigXgQQgrggg5AAIgHAAg");
	this.shape.setTransform(0.1,0);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-31.4,-22.7,63.1,45.4);


(lib.Символ6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Символ 5
	this.instance = new lib.Символ5();
	this.instance.setTransform(-23.7,-98.2,0.833,0.833);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(60).to({_off:false},0).to({scaleX:1,scaleY:1,x:-18.3,y:-104.4,alpha:1},6).to({scaleX:1.13,scaleY:1.13,x:-15.7,y:-107.8,alpha:0},7).wait(54));

	// Символ 4
	this.instance_1 = new lib.Символ4();
	this.instance_1.setTransform(-40.7,-109.9,0.833,0.833);
	this.instance_1.alpha = 0;
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(62).to({_off:false},0).to({scaleX:1,scaleY:1,x:-41.8,y:-118.5,alpha:1},6).to({scaleX:1.13,scaleY:1.13,x:-42.1,y:-123.7,alpha:0},7).wait(52));

	// Символ 3
	this.instance_2 = new lib.Символ3();
	this.instance_2.setTransform(-64.3,-108.8,0.833,0.833);
	this.instance_2.alpha = 0;
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(64).to({_off:false},0).to({scaleX:1,scaleY:1,x:-73.1,y:-117.2,alpha:1},6).to({scaleX:1.13,scaleY:1.13,x:-77.3,y:-122.2,alpha:0},7).wait(50));

	// Слой 1
	this.instance_3 = new lib.Символ2();
	this.instance_3.setTransform(-107.7,-88.3,1,1,-31.2,0,0,-28.6,-13);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(43).to({rotation:-34.2},7,cjs.Ease.get(1)).wait(1).to({regX:0.1,regY:0,rotation:-33.9,x:-76.5,y:-93.4},0).wait(1).to({rotation:-33.5,x:-76.3,y:-93},0).wait(1).to({rotation:-32.6,x:-76.1,y:-92.3},0).wait(1).to({rotation:-31.4,x:-75.6,y:-91.1},0).wait(1).to({rotation:-29.3,x:-74.9,y:-89.3},0).wait(1).to({rotation:-26.2,x:-74,y:-86.5},0).wait(1).to({rotation:-21.7,x:-72.8,y:-82.5},0).wait(1).to({rotation:-15.8,x:-71.5,y:-77.2},0).wait(1).to({rotation:-9.4,x:-70.5,y:-71.4},0).wait(1).to({regX:-28.5,regY:-13,rotation:-3,x:-99.1,y:-77.3},0).to({regX:-28.6,rotation:0,x:-99.2},5,cjs.Ease.get(1)).to({regX:-28.5,rotation:-3,x:-99.1},5,cjs.Ease.get(1)).wait(18).to({regX:-28.6,rotation:-31.2,x:-107.7,y:-88.3},9,cjs.Ease.get(1)).wait(30));

	// Слой 4
	this.instance_4 = new lib.Символ9();
	this.instance_4.setTransform(-62.4,-54.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(58).to({scaleY:0.66,y:-48.7},4,cjs.Ease.get(1)).wait(26).to({scaleY:1,y:-54.9},4,cjs.Ease.get(1)).wait(35));

	// Слой 2
	this.instance_5 = new lib.Символ7();

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(127));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-338,-212,676,425);


(lib.Символ48 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.instance = new lib.Символ6();
	this.instance.setTransform(11,0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-327,-212,676,425);


(lib.Символ49 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.instance = new lib.Символ48();
	this.instance.setTransform(348.9,212.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(22,1,676,425);


// stage content:
(lib.girl_Canvas = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		var r = this;
		var w = stage3.canvas.width;
		var h = stage3.canvas.height;
		
		if(w>h){
			
			r.clip.scaleX = h/430;
			r.clip.scaleY = h/430;
			
				if(r.clip.scaleY>=1){
					r.clip.scaleX = 1;
					r.clip.scaleY = 1;			
				}
			
		
		}else{
			
			r.clip.scaleX = w/698;
			r.clip.scaleY = w/698;	
			
				if(r.clip.scaleX>=1){
					r.clip.scaleX = 1;
					r.clip.scaleY = 1;			
				}	
			
		}
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// Слой 4
	this.clip = new lib.Символ49();

	this.timeline.addTween(cjs.Tween.get(this.clip).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(371,216,676,425);

})(lib3 = lib3||{}, images3 = images3||{}, createjs = createjs||{}, ss = ss||{});
var lib3, images3, createjs, ss;