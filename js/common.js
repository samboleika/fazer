$(document).ready(function() {    
    $('.phone-mask').mask('+7(000)000-00-00');
    
    $('.checkbox-block .checkbox').on('click', function(){
        $(this).find('input[type="checkbox"]').click();
        if($(this).find('input[type="checkbox"]').is(':checked')){
            $(this).addClass('checkbox-on');
        }
        else{
            $(this).removeClass('checkbox-on');
        }
    });
    
    if (typeof $.fn.select2 !== 'undefined') {
        $('select').select2({minimumResultsForSearch: -1});
    }
    
    $('.fancybox').fancybox({
        minWidth: 550,
        fixed: false,
        autoCenter: false,
        autoScale: false,
        overlayShow: true,
        scrolling : 'no',
        helpers : {
            overlay : {
                locked: false //Вот этот параметр
            },
            css : { 'overflow' : 'visible' }
        }
    });
    
    $('.j7-fancybox-close').on('click', function(){
        $.fancybox.close();
    });
    
    $('.gallery .search-clear-btn').on('click', function(e){
        e.preventDefault();
        $(this).closest('.search-block').find('input[type="text"]').val('');
    });
    
    $('.gallery-items .item').on('click', function(){
        var img = $(this).data('img');
        var soc_class = $(this).data('soc-class');
        var soc_link = $(this).data('soc-link');
        var name = $(this).find('.name').html();
        var popup = $('#gallery-popup');
        popup.find('.img-block img').attr('src', img);
        popup.find('.soc-link a').attr('href', soc_link);
        popup.find('.soc-link a').attr('class', soc_class);
        popup.find('.name').html(name);
        $.fancybox.open({href: '#gallery-popup', wrapCSS: 'modal-gallery'});
    });

    initAnimations();
});

function j7_modal(id){
    $.fancybox.open({href: '#' + id, wrapCSS: 'modal-'+id});
}

function initAnimations() {
    initAnimationSamolet();
    initAnimationGirl();
    initAnimationGirlWithCamera();
    initAnimationFotokonkurs();
}

function initAnimationSamolet() {
    createjs.MotionGuidePlugin.install();

    canvas1 = document.getElementById("canvas-animation-samolet");
    images1 = images1||{};

    var loader = new createjs.LoadQueue(false);
    loader.addEventListener("fileload", handleFileLoad);
    loader.addEventListener("complete", handleComplete);
    loader.loadManifest(lib1.properties.manifest);

    function handleFileLoad(evt) {
        if (evt.item.type == "image") { images1[evt.item.id] = evt.result; }
    }

    function handleComplete(evt) {
        exportRoot1 = new lib1.zovi_gostei();

        stage1 = new createjs.Stage(canvas1);
        stage1.addChild(exportRoot1);
        stage1.update();

        createjs.Ticker.setFPS(lib1.properties.fps);
        createjs.Ticker.addEventListener("tick", stage1);
    }
}

function initAnimationGirl() {
    canvas2 = document.getElementById("canvas-animation-girl");
    images2 = images2||{};

    var loader = new createjs.LoadQueue(false);
    loader.addEventListener("fileload", handleFileLoad);
    loader.addEventListener("complete", handleComplete);
    loader.loadManifest(lib2.properties.manifest);

    function handleFileLoad(evt) {
        if (evt.item.type == "image") { images2[evt.item.id] = evt.result; }
    }

    function handleComplete(evt) {
        exportRoot2 = new lib2.girl2_Canvas();

        stage2 = new createjs.Stage(canvas2);
        stage2.addChild(exportRoot2);
        stage2.update();

        createjs.Ticker.setFPS(lib2.properties.fps);
        createjs.Ticker.addEventListener("tick", stage2);
    }
}

function initAnimationGirlWithCamera() {
    canvas3 = document.getElementById("canvas-animation-girl-with-camera");
    images3 = images3||{};

    var loader = new createjs.LoadQueue(false);
    loader.addEventListener("fileload", handleFileLoad);
    loader.addEventListener("complete", handleComplete);
    loader.loadManifest(lib3.properties.manifest);

    function handleFileLoad(evt) {
        if (evt.item.type == "image") { images3[evt.item.id] = evt.result; }
    }

    function handleComplete(evt) {
        exportRoot3 = new lib3.girl_Canvas();

        stage3 = new createjs.Stage(canvas3);
        stage3.addChild(exportRoot3);
        stage3.update();

        createjs.Ticker.setFPS(lib3.properties.fps);
        createjs.Ticker.addEventListener("tick", stage3);
    }
}

function initAnimationFotokonkurs() {
    createjs.MotionGuidePlugin.install();

    canvas4 = document.getElementById("canvas-animation-fotokonkurs");
    images4 = images4||{};

    var loader = new createjs.LoadQueue(false);
    loader.addEventListener("fileload", handleFileLoad);
    loader.addEventListener("complete", handleComplete);
    loader.loadManifest(lib4.properties.manifest);

    function handleFileLoad(evt) {
        if (evt.item.type == "image") { images4[evt.item.id] = evt.result; }
    }

    function handleComplete(evt) {
        exportRoot4 = new lib4.fotokonkurs_Canvas();

        stage4 = new createjs.Stage(canvas4);
        stage4.addChild(exportRoot4);
        stage4.update();

        createjs.Ticker.setFPS(lib4.properties.fps);
        createjs.Ticker.addEventListener("tick", stage4);
    }
}

