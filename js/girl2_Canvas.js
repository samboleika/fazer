(function (lib, img, cjs, ss) {

var p; // shortcut to reference prototypes
var rect; // used to reference frame bounds

// library properties:
lib.properties = {
	width: 350,
	height: 364,
	fps: 40,
	color: "#FFFFFF",
	manifest: [
		{src:"img/girl3.png?1479944301493", id:"girl3"},
		{src:"img/girlhand.png?1479944301493", id:"girlhand"},
		{src:"img/kruassan.png?1479944301493", id:"kruassan"}
	]
};



// symbols:



(lib.girl3 = function() {
	this.initialize(img.girl3);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,270,364);


(lib.girlhand = function() {
	this.initialize(img.girlhand);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,109,139);


(lib.kruassan = function() {
	this.initialize(img.kruassan);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,212,237);


(lib.Символ9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.instance = new lib.kruassan();
	this.instance.setTransform(-106,-118);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = rect = new cjs.Rectangle(-106,-118,212,237);
p.frameBounds = [rect];


(lib.Символ8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.instance = new lib.girlhand();
	this.instance.setTransform(-54,-69);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = rect = new cjs.Rectangle(-54,-69,109,139);
p.frameBounds = [rect];


(lib.Символ6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AhHBCQgcgmAygdIA9gnQAYgQAZgLQAGABAEADQAFADADAEIAEAIQgSAlgnAUQghAUgdAcQgLALgNAAQgGAAgFgCg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = rect = new cjs.Rectangle(-8.3,-6.8,16.6,13.7);
p.frameBounds = [rect];


(lib.Символ5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("Ai6BIQBJgvBbgiQBEgdBDgkQAZgOAYgKIAIABIAHAEIAIAHIACAEQgzBHhcAnQhOAjhRAeQgQAGgNAAQgcAAgOgbg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = rect = new cjs.Rectangle(-18.7,-9.9,37.5,20);
p.frameBounds = [rect];


(lib.Символ4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AjZAxIgEgJQgCgEAAgEIACgIIABgEQBfggBugPICYgVIA+gJIABAAIANADIAHAEIAEAEQgeA9hzALQhiAKhiASQgWAEgUAAQgfAAgbgJg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = rect = new cjs.Rectangle(-22.4,-5.9,44.8,11.8);
p.frameBounds = [rect];


(lib.Символ3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AABApQhegLhMguIgBgFIgBgEIACgIIABgEQABgEADgCQACgFAEgDQBIgGBKAZQAwAQAwAGIAgAHQAWADASAMQAYARgNAYQhPgBhXgLg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = rect = new cjs.Rectangle(-17.2,-5.3,34.5,10.7);
p.frameBounds = [rect];


(lib.Символ2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AAVA4QgpgbgsgWQgigQgQgiIAAgEIAAgEIABgEIACgEIAGgGQACgDADgBIAFgBIALAAIAAgBIBOArQAnAWApASQAgAQAKAdQgVAPgXAAQgYAAgbgQg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = rect = new cjs.Rectangle(-11.6,-7.3,23.3,14.6);
p.frameBounds = [rect];


(lib.Символ7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Символ 2
	this.instance = new lib.Символ2();
	this.instance.setTransform(11,-53,0.522,0.522,0,0,0,-0.1,0);
	this.instance.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:0,scaleX:1,scaleY:1,x:4,y:-57,alpha:1},8,cjs.Ease.get(-1)).to({x:-1,y:-59,alpha:0},8).wait(49));

	// Символ 3
	this.instance_1 = new lib.Символ3();
	this.instance_1.setTransform(7,-23,0.467,0.467);
	this.instance_1.alpha = 0;
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1).to({_off:false},0).to({scaleX:1,scaleY:1,x:-6,y:-25,alpha:1},8,cjs.Ease.get(-1)).to({x:-12,y:-26,alpha:0},8).wait(48));

	// Символ 4
	this.instance_2 = new lib.Символ4();
	this.instance_2.setTransform(6,4,0.475,0.475);
	this.instance_2.alpha = 0;
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(2).to({_off:false},0).to({scaleX:1,scaleY:1,x:-11,y:7,alpha:1},8,cjs.Ease.get(-1)).to({x:-17,y:8,alpha:0},8).wait(47));

	// Символ 5
	this.instance_3 = new lib.Символ5();
	this.instance_3.setTransform(15,29,0.538,0.538,0,0,0,-0.1,0);
	this.instance_3.alpha = 0;
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(3).to({_off:false},0).to({regX:0,scaleX:1,scaleY:1,x:3,y:36,alpha:1},8,cjs.Ease.get(-1)).to({x:-3,y:40,alpha:0},8).wait(46));

	// Символ 6
	this.instance_4 = new lib.Символ6();
	this.instance_4.setTransform(33,51,0.522,0.522);
	this.instance_4.alpha = 0;
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(4).to({_off:false},0).to({scaleX:1,scaleY:1,x:25,y:57,alpha:1},8,cjs.Ease.get(-1)).to({x:22,y:61,alpha:0},8).wait(45));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = rect = new cjs.Rectangle(4.9,-56.8,12.2,7.6);
p.frameBounds = [rect, new cjs.Rectangle(-1,-56.9,18.1,36.4), new cjs.Rectangle(-4.6,-57.3,21.7,64.1), new cjs.Rectangle(-5.1,-57.8,30.2,92.3), new cjs.Rectangle(-6.4,-58.7,43.8,113.3), new cjs.Rectangle(-8.7,-59.7,46,114.5), new cjs.Rectangle(-11.8,-61,49,116.2), new cjs.Rectangle(-15.9,-62.5,52.7,118.4), new cjs.Rectangle(-20.9,-64.3,57.2,121.2), new cjs.Rectangle(-26.7,-64.5,62.6,122.8), new cjs.Rectangle(-33.4,-64.8,68.5,124.6), new cjs.Rectangle(-34.2,-65,68.5,126.8), new cjs.Rectangle(-34.9,-65.3,68.3,129.2), new cjs.Rectangle(-35.7,-65.5,68.7,129.9), new cjs.Rectangle(-36.4,-65.8,69,130.6), new cjs.Rectangle(-37.2,-66,69.4,131.4), new cjs.Rectangle(-37.9,-66.3,69.8,132.1), new cjs.Rectangle(-38.7,-66.3,70.2,132.6), new cjs.Rectangle(-39.4,-66.3,70.5,133.1), new cjs.Rectangle(-39.4,-66.3,70.2,133.6), rect=new cjs.Rectangle(-39.4,-66.3,69.8,134.1), rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect];


(lib.Символ1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.instance = new lib.Символ7("synched",0,false);
	this.instance.setTransform(-187,33);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(125));

	// Слой 4
	this.instance_1 = new lib.Символ8();
	this.instance_1.setTransform(-110,89);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(125));

	// Слой 3
	this.instance_2 = new lib.Символ9();
	this.instance_2.setTransform(-106,20);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(7).to({rotation:-2},6,cjs.Ease.get(-1)).to({regX:0.1,rotation:1,x:-105.9},4).to({regX:-0.1,regY:-0.1,rotation:-1.5,x:-106.1,y:19.9},4).to({regX:0,regY:0,rotation:3.2,x:-106,y:20},5).to({rotation:0},5,cjs.Ease.get(1)).wait(94));

	// Слой 2
	this.instance_3 = new lib.girl3();
	this.instance_3.setTransform(-165,-179);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(125));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = rect = new cjs.Rectangle(-212,-179,317,364);
p.frameBounds = [rect, rect, rect, rect, rect, rect, rect, rect, new cjs.Rectangle(-212,-179,317.1,364), new cjs.Rectangle(-213.7,-179,318.8,364), new cjs.Rectangle(-220.4,-179,325.5,364), new cjs.Rectangle(-221.2,-179,326.2,364), new cjs.Rectangle(-221.9,-179,327,364), new cjs.Rectangle(-222.7,-179,327.7,364), new cjs.Rectangle(-223.4,-179,328.5,364), new cjs.Rectangle(-224.2,-179,329.2,364), new cjs.Rectangle(-224.9,-179,330,364), new cjs.Rectangle(-225.7,-179,330.7,364), rect=new cjs.Rectangle(-226.4,-179,331.5,364), rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect];


(lib.Символ12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.instance = new lib.Символ1();
	this.instance.setTransform(245,179);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = rect = new cjs.Rectangle(33,0,317,364);
p.frameBounds = [rect];


// stage content:
(lib.girl2_Canvas = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		var r = this;
		var w = stage2.canvas.width;
		var h = stage2.canvas.height;
		
		//r.clip.x = w/2;
		//r.clip.y = h/2;
		
		
		if(w<h){
		r.clip.scaleX = w/350;
		r.clip.scaleY = w/350;
		
		if (r.clip.scaleX >= 1){
			r.clip.scaleX = 1;
			r.clip.scaleY = 1;
			}
		}
		
		else{
		r.clip.scaleX = h/364;
		r.clip.scaleY = h/364;
		
		if (r.clip.scaleY >= 1){
			r.clip.scaleX = 1;
			r.clip.scaleY = 1;
			}
		}
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// Слой 1
	this.clip = new lib.Символ12();

	this.timeline.addTween(cjs.Tween.get(this.clip).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = rect = new cjs.Rectangle(208,182,317,364);
p.frameBounds = [rect];

})(lib2 = lib2||{}, images2 = images2||{}, createjs = createjs||{}, ss = ss||{});
var lib2, images2, createjs, ss;