(function (lib, img, cjs, ss) {

var p; // shortcut to reference prototypes
var rect; // used to reference frame bounds

// library properties:
lib.properties = {
	width: 750,
	height: 180,
	fps: 40,
	color: "#CCCCCC",
	manifest: [
		{src:"img/fotokonkurs.png?1479944101692", id:"fotokonkurs"}
	]
};



// symbols:



(lib.fotokonkurs = function() {
	this.initialize(img.fotokonkurs);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,486,94);


(lib.Символ45 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("EAj9AMUQg7gDgZgHIgagIQgPgEgLgCQgQgCgoADQgrAEhFAAQgtAAgVgIIgcgKQgJgBgOADQgPAFgHABQgTADgXgFQgJgCgLgEQgIAGgQgCIjugSQhggHiEgHQhlgFgWgEQg1AGgOACQhSAMh7gMQiJgRhFgEQhZgDgtgDQgcgBg2gGIgqgFQgEADgFABQgJADgOgCQiPgLjlADQkLADhUgBQgJADgQABQoCAEnzhpIgEAEIgDACIgCAAIgIAAQgDgBgEgDIgCgCIgBgCIgCgCIgCgCIgZgGQh3gahCgaQhDgag5gkQgGgDgDgEIgJAHQgMAIgZAFIicAgIgRADQgoAHgMABQgiADhlASQjJAmjTAYIgKAAIgYABIgaADIgkADQggACg/ABQiSAChJgDQhKgEglgBIgtAAQgagBgTgCQgfgEgYgKIgEAAQgDAAgGgDIgWgLIgFgDIgCABQgVAEgWgBQghgCglgRQgZgMgngZIhCgsIgTgQQgRgSgJgwIgBgGIgFgFQgFgHgCgLIgThyQgLg/gBghIgDhWQgChBABgLQACgMgBgMIgCgUQgCgnADhMIAQmTQACghALgQQgDgGAEgFIABgCQABgDAFgCQAWgIAugHIApgGIBQgEIAFAAIgKgxQgGgRAAgMQgBgKAEgHQAGgJAKAAIAAgBIAbAAQA7gLBsgCIBigDIAAAAQAUgCAoAGQAGABAKAJQANAWgGAyQCYgCCZAAIAAgDIBGABQCdADBVAFQCtAKBwAPQBxAPAjAIIAGABIABAAIABgBQAZAEAgAbIAOAMQgDgGA7gFIDMgfQDXghBqgLQCzgTCQAEIAAgCQD6AKD2A9QANADANAGQAjAIAxAPQAoAMBOAaQDcBHD1AfIAvAFIAQgCQAQgBAIADIAJAEIAMACQASACALgBIANAAIAHADIAMACIACABQCFANBkAAIDagCIBagDIApgHQAHgBACACIAGADQAEACAUABIAbAAIAAAAIARgFQAMgEAFABIANACQAEAAAHgEQAGgEAEABIAAAAQAHgCAHACQAHADAEAGIAAABIACAAQAcgDApgJIBEgOIAUgDIAIgCQADAAACgCIAEgDQADgCAHgBIAVgEIALABQAEAAADADIAZgDQA2gIA7gMQAAAAABAAQAAAAABAAQAAgBABABQABAAAAAAIAEgBIAagFIADgBIgTAEIBMgYQBDgUAggHQAbgHAvgIIAugSQAYgJAQgCIAfgDQAPgCAXgKQAbgLALgDQARgDAXgBIBPgDQAWgDATgBQAMgBAIAAQAJgDAOgCQCMgTCOAAIAAgCQA1ABA0ACIAbgLIABAAQAGADANAAIAlADIAuAFIAUABIAcAFIAUAEIAEABIBiAJIAuAGQApAIBHAKIF7BBQAeAFANAHQAXAMADAWQABAKgDAHQAEAuABAyQAAAugGBQQgIBagBAjQgCBVALBkQAJBIATBwQAEARACAUIAMArQAJAhAGAgIAFAXQAWBkAGAzIAEAZIADALIAAACIAEAEQAFAKgEAIQgEAIgOAHIhRApQgyAagcAHQgoAGgTAGIgYAJQgOAEgKABIgMABQgDADgFACQgHAEgYAAQgkAAgiAKIgXAHQgNACgKgCQgOgHgIgBQgHgBgPAGQgZAIg0AFQhQAHgpABIg9AAQgkABgZAEQgoAKgVADQgjAGhCgFQhGgFgfAEIgWACIgOABIgCAAQgKAFgNAAQgZAAgYgMQgNgGgFAAQgEABgGADIgJAGQgLAGgQACIgRABIgNAAgEAldALeIAcAFIARABQALAAAGACIAJAEQAsgKArACQARAAArAEQAmADAWgBQAWgBATgEIArgLQAWgFAeAAIA1ABQAaABBIgFQAegBAXgEQAXgDArgMQAYgHALAIIAHAGQAEAEADABQAGADAIgDIANgHQAUgMAcgDIAegCQAFgEAJgCQAIgCAPAAIAYgBQAKgBAZgJQAOgEAlgIQAggHARgGIAfgOQA1gcAWgMIAJgGQgEgQgEgZIgGgqIgHggQgMgygLgmIgIgaIgShWQgEgXgBgQQgBgRgGghIgJg3QgKg+gJh9QgFhCABghQAAgbAFg2QAKiDgHh3IAAgFIgGgLQgGgHgRgEIg9gIQhFgHiJgdQiJgdhGgHIg4gGIgOgBQizgTiMgDQisgEiUASQggAFgWAAIgFADIAAAAIgCADQgCADgFACIgIABIgLgBIgKgDQg5AEhUATQiCAfgVAEQg2AIgcAFQgfAHhgAcIhAARIAOgDQApgIhKATQhKASg7AIIhgANQhLASgnAGQgjAFguACIhTACIjBAAIgNAAIhMAFQk0AEkxg1QisgeiegsIgBAAIgDADQgDACgJgCQgFgBgDgCQgCgDAAgFQhigchbghQgpgNgTgDQgUgDh7gdQh6gekdACQkeAClPA+IhGANQgoAGggACQgWACgPgBIAUASIAoAeQAXASANAQQAGAHABAGQAEABADAFQACADACALIAGA0IADAWIAAASIACAOIgBAJIgCATIgBAFIACAIQACAOgBAXIgLDGQgCA3ABASIACAiQABAUgBAPIgDAhIgEAiQgBANABAUIACAhQABAWgCAsIgEBaQgCAdgDAQIAAABIABACQAEAHAAAKQAAAGgCAFQBTAoArASQBMAgBBAOQAfAHA0AHIBUALQAvAIBdAWQBXASBtAMIAHgFIAMgCIALAAQAOgCAPAIQAEACAEAEIABABIABAAIAAAAIAQgEIAKgBIACgBQAIgBAGABIAHADIAGABIAJAGIAFACIBDAFIACgCQACgCAEABIAAgBQAZADAZAAIAJABIAEAFQBfAGAyABQBuAFBXgFQAPgBAEACQAEACARgEQAQgFARAHQASAGAggBIA3gEQBRgFCoAEQClADBWgGIAAABQAbAAAiACIAIABQAHgCAmABQA1AJBLAEQCJAHAUACIBOAJQAzAGBEgCIB5gFIB0gEIgZgDIJXAwQACgDAEgCQAJgDARADIABAAICUAMIAvAAQAggBApACQBRACAaACIBBAHQApAFAYACQAUABAMgDQAGgBALgFQAKgEAGgBIAJgBQAJAAALADgEgzfgCHQAEC1AEBbQAHCYAPB4IAAAEIAIAnIAAASQABAWAJAgQAEAMAEAFQAFABADADQAkAVA0AGQAfAEBFAAQBSAACkAFQBxACBWgJIAugFQAMgCAWABQAVAAAXgDIBVgMIBggNQApgGBsgWQBagSA3gHQAGgEAMgBQB1gLBdgXQAagHADgOIABgMQAAgIACgEIABgCIAOnEQAEieAEhRIgBAAQgWAHgiAGIgOADIghAFQgbADgagCIgGgBIgEAOQgIARgOALQgLAIgNACIgNAAQgMAAgFgCQgEgCgEgFIgJgIQgIgJgDgHIgCAAIgjAEIg3ADQghACgVAEIgXAEQgJABgSAAIgqAAQApAnAoBCQAfA2AMAlQATA3gHA/QgGA9gcA3QgbA1guAsQgsApg4AcQhXAqhgADQg1ABhAgMIgCAAQgpgIg8gQQg+gSgggSQghgSgNgPQgNgPgcgQQgcgQgXgqQgRgggJgxQgHgoADgdQACgXARg8IAKgkQAfhLAVgbQAOgQAdgYIABgBIgSACIg4AFQgiAChEAAIiJAAQg0AAgegFQgjgFgdgMIgBgBQgBAfACAsgEg3jgDYIgDCjIABAmIACA8IAEBEQABAgAFASQAEASADAbIAFAoIAFAyQACAfAHATQAFAPAAAHIAAABIAGAQIAIAZQAGAOAIAIQAGAHALAHIAyAjQAvAiAcAMQArATAkgEIgGgVIgFgXQgCgMgBgLIAAgTIgFgZQgDgOABgNQgfjIgDjzQgBhQAChmQgHAAgLgEQgcgKgLgFIgqgZIgBAHIgEAIQgEAIgFAFQgIAHgMACQgJABgNgDIgLgEIgOgLIgGgIQgEgHAAgQQAAgLABgEQACgGAJgIIAKgKIgDgDIg5g7QgHBWgBBJgEgsdABTIgGBIQgFA+AFAPQAFAPAZAXQAYAXAbAOQAVANAyAMQA5AOAsAIQAqAIAeACQBYAFBTgaQBDgVAsgkQA4guAohVQARgnAEgbQAEgZgGgnQgKhCgcgzQgQgcgjgtQghgpgYgRQgggXg1gNIhQgSIgHABIgHgDQhQADg7AQIgFACIgGAEIgcANQgWAMgVANIgGAFQgSAPgHAJIgVAfQgQAXgIAPIgBADIgDAHIgQAqQgSAwABAvQAAA0AVAsQARAjAeAcQAeAcAlAPQAZAKAdAEIABgCIAGgGQAZgXAVgkQAOgYATgsIANgfQAIgWAFgoQAIguABgcQABgqgOgfQgHgQAAgIQABgHAFgEQAFgFAFACIABgBIABAAIACAAIAAgBIABABQAEAAAEADQAQAIAMAMQATATAMAeQAkBPgJBcQgIBQgqBNQAggKAZgOQA+gkAhg/QASglADgiQAEgzgeg0QgcgvgwgiQgXgQgXgLQg1gbg3gFQgsgEgmALQgNAEgNAGQgRAHgFABQgGAAgFgCQgGgDgCgGQgEgOAXgMQAXgMAZgHQAcgHAegBIgBAAQBGgBBHAgQAnASAfAYQAyAnAcA2QAdA5gDA5QgEBNg4BBQgtA1hAAZQgmAPgyAHQgzAIgogFIgYgEQgXgFgjgKQgogMgQgKQgWgMgggiQghgigLgYQgIgVgEgpQgFgmABgZQgOAogFAhgEgmNgAqQgCAqgPBBQgDARgEAKQgEAOgRAdIghA4QgLAUgLAOQgLANgMAKQAjABAogFIAVgEQABgFAEgJQAbg4AKgiQAQgzgBg3QgBg0gQgzQgIgZgIgMIgBgBQAGAhgCAkgEgq7gDeIgMAIQAAAFgEAEQgEAFgHACIgHABQgPANgRARQgnAlgLATIgQAcQgGAJgEAMIgRA+QgOAvgCATQgCAbAGAiQAGAlALAZIAIAQIAEgJQAEgNAAgcQAChwAohmQAZhDAjguQAggoAtgeQgXAJgSALgEgsCgIeQhNABhlAHIiyAPIhFAFQgRABgTALIACALQADAQgDAnQgEAmgIAwQgGAugBA5QAbARAoAGQAcAEAxgBQBdAABDgCQAlgCAvgEIA7gGQAigDAYABIAIABIAXgQQBGgqBcgNIAAABIAAAAIA1gIIBSgFIAAgBQBEAKAlAJQA0ANAmAXIAHgCQARgEApAAQAaAAANgCIAkgGQATgDAugCQAqgCAXgFIAAABQAHgDAIACIACgHIACgLIAEgLQAFgKAGgDQAFgEAKgDIAQgDIAAgBQAQABAPAKQANALAHAQIABADIALgBQAXgBASgDQAkgEBGgPIAAAAIAJAAIAAgCIACgHIgBgIIgDgIIAAgPIgBgPIgHg4QgGgBgJgDQgagIgYgDIgEgEQgVAAgUgGQgQgDgIgEIgrgNIgLgFIiSgaQhUgOhBgFQhGgGhmABQhyACg6gBIgvgBIhXgCQhEgCgvAAIgSAAgEg3IgJBQgKBVgGBMIAEAEIAMAMQAQARATAPIAdAVIAVATQAJAIATAMQA7AkAcAMIAKAFQADhuAGiEIgEgDQgbgVhAgjIgjgSIhYgiQACANgDASgA+RkqQgBAAAAAAQgBAAAAABQgBAAAAAAQAAAAgBABIAAACIgCAFIgJAQIgBADIABADQABADADACIAFAEIADABIACAAIAFgBQAGgCACgEIAEgGIADgIIAAgEIgEgFIgCgFQAAgBgBgBQAAAAAAgBQgBAAAAgBQAAAAAAAAIgHgCIAAgBIgEABgEg2NgEXIgCAFQAAAAgBAAQAAAAAAABQAAAAAAAAQAAABAAAAIACADIAEAEIAFABIADAAQACAAABgEQgFgHABgJIgCAAIAAAAQgCACgGADgA7NneIgFgEIgjgbIgjgaQgOgLgXgWIglggIgXgPIgagFIhfgMQibgThNgIQiBgNhngCQglgBg/ABIiLAEQiYAFhWgEIgIgBIgFALIgQAmIgEAIQgGAHgJgBQgIAAgGgIQgDgHABgJQABgEACAIIAIgeIAHgOIALgoIgBgEIgEgkIgBgCQgRgDggABIh5AEIg+ADIhWAJIACAHQAEALADAYIAJBIIABARQgBAHgCADQgDAEgFABQgFABgEgDQgGgEgBgOIgBgOIgDgQIgDABQgSADgiACQglABgPACIgyAIIAqAPIAZAKQAUAIAoAVIA2AaQAOAGBDgNQBCgNE6gLIgbACQCmgCB8ABQB/AABTAEQEVALDZArQALACAGAEIAPADIAXAHIAkALQAVAGAGAAIAjAHIAAAAgA+JqFIABgBIACAAIgDAAQgBAAAAAAQgBAAAAAAQAAAAABAAQAAAAABABgEgw2AIkQgIgEgIgIIgEgGIgBgGIABgIIAEgGIAEgEIAIgCIABAAQAFAAAEACIAJAGIAGADIADAEIACAEIABAEIAAAHQAAAKgIAFQgFACgEAAQgFAAgFgDgEgtiAIXIgFgCIgEgDIgEgEIgCgEIgBgFIABgGQAAgDADgEIADgDIACgFQADgJAKgBIACAAQAJAAAEAGIACAHIABAJIABAGIAAAGIgBACIgCAGIgFAEIgFADIgGABgEgv3AHhIgDgBIgFgCIgIgHQgKgIgEgFIgCgEIAAgFIABgIIADgEQAGgGAGACIAAgCIAAABQAGgCAIAEIALAKIAHAIQADAFAAAFQABAGgEAGIgHAGIgDABgA/VFnQgMgCgHgJQgFgGgCgOQgCgSABgJQACgQAQgbIBJiBQAMgVALgMQAQgQATgBIAAgBQALgBALAGQAKAGAHAJQAMASgBAbQgBAbgOAfQgJAUgWAiIggAyQgXAegRAMQgUAOgUAAQgHAAgHgCgA9eB7IABAAQgRAOgKATIgKATQgEAIgMAPIgJAUQgJAXgPAWQgQAYgBAEQgIARAFAOQANgDAKgIQAHgHAHgJIASgbQAig3ALgVQAKgTADgMQAEgSgEgPIgDgFIgCgBIgDABgEgvFAEqIgIgBIgLgIQgFgEgBgEIgBgJQABgIACgCIADgDIADgEIAFgEQADgCADAAQABAAAAAAQAAAAABAAQAAAAAAAAQABAAAAAAQAMABAFAJIADAGIAEALQABALgIAHIgIAEgEgxNAEoQgLgCgHgIIgDgDIgDgGIgBgFQgBgJACgEIAEgGQAGgFAGABIAAgBIABAAQAFAAAFACIAFADIAFAFIADAEIAGAKIACAHIgCAGIgDAGIgEADIgFACIgDAAgEgwlACUIgFgCQgFgDgGgFIgCgDIgCgEIgBgHIABgIIADgEQADgEAEgCQAEgDAFABIABgBQAGAAAGAEIALAHQAHAFABAJQAAAIgFAFIgFAEQgDACgEABIgKABgEgxmAAHIgGgDQgFgDgCgCQgCgDgBgOIABgIIADgEIADgDIACgCIAEgEIAHgFQAEgDAEAAIAAAAIABAAQALgBAGAGIAEAEQAHAPgJANIgEAFIgCAFQgDAFgIADIgHABQgEAAgEgCgEgvngACQgIgDgBgHIgBgHIAAgLIABgGIABgCIAAgDIADgLIAAgFIAEgHIAHgGQAFgEAEACIABgCIAGADQAJADAEAEQACAEABAOIAAAJIgCAFIgHAPIgEAHIgMAHQgDACgEAAIgGgBg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = rect = new cjs.Rectangle(-358.5,-78.8,717.1,157.6);
p.frameBounds = [rect];


(lib.Символ44 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AhSBKQgFgIAAgXQAAgggLgcQgFgOAAgGQAAgGAEgGQAGgJANgDQAKgCANABQASABAIgBQAGgBAXgJIAVgHIAXgHIABABQAagCAJAJQAFAFAEANIANApQAIAXgHAMQgDAFgHAGIgQAOQgRAPgKAFQgMAGgTAEIgdAGIgSAGQgLADgIABIgHAAQgSAAgIgNgAg0gLQAEALABAGIAAAIIADAMIABAOQAEgBAIgEIAXgGIAKgCQAIAAAEgCIAKgEIAHgEIAUgQQAEgCABgDQABgDgCgGQgEgNgFgMIgCABIgBAAIgeALIgKADQgFACgHAEIgJAEQgGACgLAAIgHAAIgKAAg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = rect = new cjs.Rectangle(-10.4,-8.7,20.9,17.6);
p.frameBounds = [rect];


(lib.Символ43 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AhiBAQgOgMgCgUIgBgQQAAgKgDgGIgIgLQgFgJACgJQACgKAIgGQAEgEAMgDQAOgDAMgCQAHgBAYAAIAaAAIAVgBQAIgBAXgHIAlgNIAAAAQAZAAAKAJQAIAHAGASQAOAogCAbQgCAVgKAKQgIAKgTAEIgfAFIgUADIgoAEQgTACgVAAQgpAAgRgQgAAOgIIgIABIhBABQgOAAgFAGQABARAJARQAZAHAmgJQAKgEAHAAIATgBIAOgEIATgDQAMgBAHgCIgCgRQgCgLgGgOIgCgDQAAAAgBAAQAAAAAAAAQgBAAAAAAQAAAAgBABIAAgBQgQAIgmAMg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = rect = new cjs.Rectangle(-13,-8.1,26.2,16.2);
p.frameBounds = [rect];


(lib.Символ42 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AAtBPIgcgCQgPgCgMgDQghgIgRgCIgdgBQgTAAgLgCQgNgCgFgIQgFgHABgNQAAgOAEgbIADgWQADgLACgEQAGgKARgEQATgFAagCIAtgCQAogBAjgFIABAAQAaACAQAGQAXAKAJATQAEAIACAOQADASgBArQgBAOgCAEQgHAOgSADQgGACgZAAIgcAAIgKAAgABAgcIAAAAIhpAIQgoACgSAJQACAKgDANQAAABAAAAQgBABAAAAQAAAAABABQAAAAAAAAIACABIAsABQAMABAhAIQAyAMA5gGQABgYgEgWQgBgGgCgDQgCgCgGgBQgLgFgGAAIgDABg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = rect = new cjs.Rectangle(-14.2,-7.9,28.6,16);
p.frameBounds = [rect];


(lib.Символ41 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgRBFIg8gJQgOgCgHgFQgGgEgFgMQgGgPABgQIACgMQABgJgBgGQgDgMABgHQACgJAKgGQAJgEALgBQASgDAPABIAMABIAMABIAPgDIANgDQAJgCAUABQASAAAKgCIAAABQAggCANAOQAKALAAAaQAAATgDAbQgDAPgDAHQgGANgKAEQgGACgKgBIgRgBIgYADIgdABgAhJgIIAAAZIAbAFIAOACIARADIAsAAIAJgDQAMgDAMADQADABACgBQADgBgBgFIgBgJIABgIQADgJgCgKIgHgBQgKACgWAAQgUAAgLADQgFADgFABIgMABIgYgBIgOgCQgJAAgEAEg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = rect = new cjs.Rectangle(-11.5,-7,23.2,14.1);
p.frameBounds = [rect];


(lib.Символ40 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AhBBFQgKgGgEgPQgCgJABgPQAAgSABgGQACgMgBgDIgFgGQgIgJADgMQABgLALgGQAFgDAQgCQASgDAMgEIAAAAIA4ABQASABAJAEQAKAFAHAJIAIALIADAOQAEAUgBAcQAAAMgCAJQgEALgHAGQgIAFgXAAQgPAAgeAEQgeADgPAAQgNAAgHgDgAgRgRQgHADgDAAIAAABIgGACIABALIgBAFIgBADIgBAGIABAKIAdgDQAagEAVAAQABgNgCgIIgDgPQgBAAAAAAQAAAAgBAAQAAABAAAAQgBAAAAABIgOABIgHgBIgEAAIgXAAg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = rect = new cjs.Rectangle(-9,-7.2,18,14.5);
p.frameBounds = [rect];


(lib.Символ39 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgEBHIgXgGIgOgBIgPgBQgKgCgIgFQgKgHgDgGQgEgNAHgQIAGgOIAFgMQACgGgBgFIgCgLQgCgHADgHQADgHAHgFQAFgDAKgBIAQgBIAQgEQAQgEAgABIABgBQATAAAJADQAQAEAHANQAHAKgCAaIgCAXQgCAbgHANQgLAWgYAEQgHACgQAAQgUgBgEgCgAAlgUQgGACgJABIgPAAIgJABIgNAFQgEACgGAAIgBAAIgCACIAAAHIgCAIIgEAJIANABQAJAAAEACIAGADQACACAKABQALABAJgBQAGgBABgEIAAgDIACgKIAAgIIAAgKQAAgGACgEIgBgBIgDABg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = rect = new cjs.Rectangle(-9,-7.4,18,14.9);
p.frameBounds = [rect];


(lib.Символ38 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("Ag5A4QgPgEgFgEQgKgIgDgQQgCgJABgQIAEgfIABgMQACgHAIgEQAGgEAJAAQAGgBAJACIAQAEQAPACAPgDIASgEIANAAQAIAAAFgCQAUgBAIAFQAJAFAFAMQAFAMABAcQAAAWgCAKQgFATgNAGIgMACQgfAEgaAAQgiAAgagHgAg0AQQAGACAMABIANAAIAGABIAIABIAvgEQAGgBABgCIACgDQACgEAAgHQABgLgBgKIgDABIAAAAQgSACgOADIgQAEQgEACgOAAIgWgBIgFgBIgJgBQAAANACAPg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = rect = new cjs.Rectangle(-9.2,-6.3,18.6,12.6);
p.frameBounds = [rect];


(lib.Символ37 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AA3BOQgOgEgHgBIgmgGQgIgCgPAAIgXgCQgbgDgMgRQgEgIgDgRQgCgRAAgfIABgRQACgLAKgIQAIgJAMgCIAAgBIBNALQAVAFAbANQASAHAIAKQAJAKADASQACAKgBASQAAAPgCAHQgCAKgGAJQgGAJgGADQgGACgHAAIgJgBgAgxgaQADALgHAKIgCAEIAFARQALACAWABIAOABIALACQASAEAbACQABgQgBgIIgDgFQgCgDgFgCQgdgNgQgCIgvgFg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = rect = new cjs.Rectangle(-10,-7.9,20,15.9);
p.frameBounds = [rect];


(lib.Символ36 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AAyBIIgTgBIgTgBQgSgCgigKQgSgFgIgFQgOgIgFgLQgDgGAAgLIgBgPIgCgQIAAgQQgBgLAAgFQABgIAHgGQAHgGAIAAIAAgBIAsAGIAhAFIAcAEQAQAEAIAKQAEAEAFANIANAeQAJATABANQADAZgQAJQgGADgLAAIgMgBgAg4gVQADAEAAAHIAAALIACAJIACAEIAEACIAtALQAOADAggBQgFgHgCgFQgEgNgDgEIgDgHIgCgFIgDgBIgmgDIgMgEQgSABgJgDIgBgBQgBAAAAAAQAAABAAAAQgBAAAAAAQAAABAAAAg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = rect = new cjs.Rectangle(-9.3,-7.3,18.7,14.6);
p.frameBounds = [rect];


(lib.Символ35 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgRA+QgQgCgFgEQgOgGgHgPQgGgLgBgQIgBgIIgIgDQgGgEgDgKQgBgIACgKQACgMAFgFQAEgFAKgDQAVgHAWAEIAAgCQAKADALAAIAYABQAfADAKARQADAGACAJIAEAQIAGAPQAEAMgBANQgBAVgOAIIgNADQgcAEgOAAQgJAAgXgEgAAxAeQgPgQgDgJIgCgFIAAgCIgBgDQgCgEABgEQgZgBgKgKIgPAHIgFgCIAAACIAAAAQgBADgEABIAGAFIAAAGQgBAHAAAOQAAANAGgGQAHgIAQAIIADgHIAIABQAEABAIgCQAIgBARAMIAAAAg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = rect = new cjs.Rectangle(-8.5,-6.6,17.2,13.3);
p.frameBounds = [rect];


(lib.Символ34 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgFBCQgggFgUgXQgIgLgCgMQgEgVgHgVQgFgRAOgIQAGgFAJgBQAHgCAJAAQAcACAZgIIAdACQATACAHATQARAtgLAuIgDAJQgIAHgNAAQgSADgSAAIgVgBgAgYAcIBGABIgRgwIgIgCIgPACQgVACgYgJg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = rect = new cjs.Rectangle(-8.1,-6.7,16.2,13.5);
p.frameBounds = [rect];


(lib.Символ33 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AAJBBQgagEgVgBIgNAAIgOgCQgRgFgEgMQgCgMgEgFIgJgKQgIgIgBgWQAAgWAFgIQAJgOAdgFIABAAQAXgCAhAEIA2AHIAbAEQAPAEAGAJQAFAGAEAPQAJAcAAAKQABALgFAJQgEAKgJAFQgGADgMAAIgSACIgUAGIgJABIgTgCgAgQAUQALABALACQAQADAGgBIAQgFQANgDALAAIgHgUQgBgEgCgCQgCgBgEAAIgfgGIgRABIgRgCIgSgBQgWgDgGACIAAAAIgFACQAIATADASIATAAIASAAg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = rect = new cjs.Rectangle(-11.1,-6.7,22.2,13.5);
p.frameBounds = [rect];


(lib.Символ32 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AhPBHQgdgDgJgPIgEgLIgDgNIgHgLQgEgGgCgEQgCgHAAgLIACgVQAEgQAOgLQAOgLARAAIAAgCQAQACArAAQAjABAVADIAtAEIAZACQAPADAHAJQAGAGADAKIADASIAFAVQADAMABAJQACAbgUALQgJAEgSAAIg+gFIgOAAIgRAEQgHACgTAAQgkAAgTgBgAhSgbQgJAAgDAJQgDAEACAIQADAOAFAMQADAGACABIAFACQAQACAhAAIAUgCIAKgDQAPgEAeACIAyADQgHgUgDgTQAAgBAAAAQAAgBgBgBQAAAAAAgBQgBAAAAgBQgCgBgEAAQgbABgQgDIgRgEQgLgCgNAAIgbABQgYgBgZgCg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = rect = new cjs.Rectangle(-14,-7.2,28.1,14.5);
p.frameBounds = [rect];


(lib.Символ31 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AAsBLIgPgDIgYgBQgKgBgXgGIgpgMQgcgJgJgOQgIgOAFgeQACgMAAgGIgDgLQgBgIAAgEQABgJAHgGQAHgGAIAAIABAAQAPAAAbAJQAdAKANABIAPACIAQABQAOABAbAHIASAEQASAHAFASQACAHAAANIAAAqQAAARgFAGQgGAHgLACQgJABgJAAQgNAAgPgDgAApAdIAPAEQAIABAGgBIAAgfIgBgDQgCgCgFAAQgIAAgNgDIgVgEIgMgBQgWAAgNgEIgQgFIgMgDIgQgEIgBABQABAJgBAIIgCAJQAAADADADQADADAGABIAYAFIAhAJQAKAEAFABQAGABAKgBIAJAAIAGAAg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = rect = new cjs.Rectangle(-11.2,-7.9,22.5,15.8);
p.frameBounds = [rect];


(lib.Символ30 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AAfBPIgWgGQgKgEgWgLIgVgLQgUgLgEgDQgMgKgDgMQgDgIACgKIACgUIAAgXQACgMAJgIQAIgJALgCIAAAAQALAAAZAGIAjAHQAZAEAMAHQASAMAHAZQAGASgBAZIgBAUQgCAagNAJQgGAEgMAAQgJAAgMgDgAgjgbQgBAFgFAJQgCADAAADQAAAFAEACIAJAFIAIAFQAFADALADIAKAIIASAHQAKADALACIABgTIgBgMIgEgNQgCgGgDgDIgGgBIgTgDIgRgEIgLAAQgHAAgEgBIgFgBg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = rect = new cjs.Rectangle(-8.6,-8.2,17.3,16.6);
p.frameBounds = [rect];


(lib.Символ29 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgHBQQgTgCgSgLQgIgFgDgFQgFgHABgHQABgGAGgEQAFgEAHACIAMAFIALABIALADIAOgEIANgDIAOgCQAJgDADAAQAMgBAFAKQAHAJgGALQgEAIgJAEQgIACgKABQgNABgEACIgJADQgGACgEAAIgFAAgAhFAXQgGgFgCgHIAAgLIgDgIIAAgRIAAgSQAAgLAHgEIAKgFIAKgGQAKgFARgBIAwgDIABgBQAOABAMADQAHACAEADQAFADAFAHQAFAIAEANIACAkQAAAOgDAFQgDAFgFADQgHACgFgBQgIgCgEgHQgCgEAAgKIABgQQAAgLgBgHQAAgFgDgEQgCgCgFgBQgLgCgPACIgXADIgRABQgKABgJAGQgCAJAEATIAAAKIACAKQgBAFgIAGIgDABQgFAAgFgEg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = rect = new cjs.Rectangle(-8.2,-8.1,16.4,16.2);
p.frameBounds = [rect];


(lib.Символ28 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AArBMIgrgCQgXgCgRABIgUgBQgMgCgFgJIgCgLIgHgtQgBgJgDgGQgIgMgBgHQgDgNAPgLQAJgGAPgEIAagEIAWgFQAMgDAIAAIAAgBQAHABALAAIASgBQAWAAANAJQAQAMABAWIAAAQIAEAPQAGAXgDAcQgCAPgGAGQgHAGgSABgAAJAfQAlACASgCQACgIgEgNQgFgOAAgFIAAgQIgCgEQgBAAAAgBQgBAAAAAAQgBAAAAAAQgBAAAAAAIgVABQgeABgPAFIgNADIgNAAQgIABgFACIAHAbIAAAHIACAHIABAHQAKgBAPAAIAcABg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = rect = new cjs.Rectangle(-10.1,-7.7,20.3,15.5);
p.frameBounds = [rect];


(lib.Символ27 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AANBNQgqgFgYgIQgRgFgHgIQgGgHgFgPIgSg4QgJgaAJgLQAFgIANgDIAWgEQAJgCALACIATAFQAOADAlAEIAjAEQAVACAIAGQAJAHAFAMQAMAdgEApQgCAWgIAKQgMAOgbABIgEAAQgQAAgcgEgAARAhIAQADIAdAAIAEgBQADgBAAgDQACgMAAgJQgBgJgFgQQgBgEgDgCIgDAAQgpgGgLgBQgdgCgLgDIgOgEQgIgCgGABIAAAAIgGABIAAAGIgBAGQAAACADADQACADAEALIAHATQADAIAFADQAEACAJAAIANADQAHACAMgBIARADg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = rect = new cjs.Rectangle(-11.2,-8.1,22.4,16.3);
p.frameBounds = [rect];


(lib.Символ26 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AA9BcIgTgLQgQgKgfgIIgfgHIgUgCQgYgGgIgWQgCgGgDgUIgDgyQgBgTAEgJQACgGAGgFQAGgFAGABIAAgBQAOgCASAJIAcAQQANAHAbAHQAeAHAMAGQANAGAFAGQAHAIACALIABAUIAAAyQAAAOgCAGQgEAMgJAEQgEACgFAAQgFAAgHgDgAgzgfQAAAKgDAFIgDAGQgCAHAEAJIADAGQACADAFAAIAMAAIAMABIANAEIAJABQAEABAIAFQAGACAOACIAMAFQAHAEAFAAIAAgnQAAgDgBgBQgBgCgEgCQgMgFgOABQAAAAAAAAQAAgBgBAAQAAAAAAAAQgBAAAAAAIgIgDIgHgDIgHAAIgHgEIgHgBQgEAAgPgIIgWgLQACACABAJg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = rect = new cjs.Rectangle(-9.7,-9.6,19.5,19.2);
p.frameBounds = [rect];


(lib.Символ25 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("ABaBSQgNAAgHgBQgHAAgPgGQgZgIgRgEQgMgDghgDQgegDgQgEQgGAJgJABQgKACgGgIQgEgFAAgPQgCgiABgOQAAgdAJgTQAHgPAMgFIAAACQAVgFASAFQARAGARADQARAEAHAFQAJAFASAEQASADASAMQAIAFAOAPIAJAMQADAGAAAKQABAYAGAIIAHAIQAEAEABADQAFAJgFAJQgFAIgKABIgBAAIgOgCgAhWgnQgEAQABAKQAAAJAEAPQAtAHAOABIAaADIAZAGIAnALQABgDgBgEIgCgIQABgIgCgEQAAgDgEgEIgLgIQgEgDgLgCIgQgDQgYgLgIgCQgHgCABACIAAgBQgBADgYgHQgggIgHgDg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = rect = new cjs.Rectangle(-12.3,-8.4,24.6,16.9);
p.frameBounds = [rect];


(lib.Символ24 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AA0A/QghgBgPgCIgQgFIgdgJQgHgCgIgEQgJgEgHgGQgIgGgDgHIgJgWIgFgUIAAgQQABgJADgFQAFgIAIABIAAAAQALgBAEAKQAnAIATAGQALAFAGAAQAHgBANgFQAJgCANADQANADAGAFQAGAEAJASIAIAQQAEAMAAAFIgBAKQgCANgHAHQgJAJgXAAIgEAAgAAkAhQACgIAQgBIgKgTQgCgDgFABIgHABIgKgCIAJgHQgZAJAHgFIghgBQgQgGgPgBIgBAAQADACABAGIAEACIADAEIgFAKQAcgCgPAGIAbADIAPgBg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = rect = new cjs.Rectangle(-9.8,-6.3,19.8,12.7);
p.frameBounds = [rect];


(lib.Символ23 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AAwA/QgYgCgNABIgLACIgLABIgTgEIgPgCQgJgBgFgCQgJgEgKgOQgHgKgEgJIgEgQQgFgDgEgHQgEgJgBgRQAAgJACgFQACgHAGgFQAHgEAIgBIAAgBQAZACAgAJIARAFQAOAFAGAAIALAAIADgBQAGAAAHACIAHADIAFAEQADAEgCAGQAAABAAAAQAAABAAAAQAAABgBAAQAAABAAAAIgFAFQgDAFgFACIgEACIgDAAIgJAAIgEAAIgEgCIgFABQgJABgWgEQgYgDgKABQACAGgCAGIAJAQQACAEADACQADACAKAAQAGAAANADQAHABASgDQAWgCAZAEQgGgKgCgHQgDgJAEgIQAEgIAKgDQAKgBAHAFQAFAFAEALQAMAZABAPQABANgHAHQgFAEgMABIgEABIghgDg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = rect = new cjs.Rectangle(-10.8,-6.6,21.7,13.2);
p.frameBounds = [rect];


(lib.Символ22 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgQBNQgXAAgMgDQgOgDgKgHQgNgIgFgNQgBgOgEgGIgIgMQgDgEgCgKQgGgWgBgLQgCgVAMgKQAGgEAOgDIAAgCQA7AGAggBIAlAAQAXACAbALQANAFAGAGQALALAAATQAAAMgDAUIAAARQAAAJgBAHQgDALgGAEQgFADgJACIgPABQgLAGgGABIgMABgAApAmIAIAAQAEAAAGgDIAPgFQABAAAAAAQABAAAAAAQABgBAAAAQABAAAAgBIABgEIAAgYQAAgGABgDQABgBAAgBQAAgBAAAAQABgBgBAAQAAgBAAAAQAAAAAAgBQgBAAAAAAQAAgBgBAAQgBAAAAgBIgUgFQgNgEgLAAIgTAAQgPACgagCIgrgBIAAABIACAJIABAKQAAAGAFALQAEANABAGQAJAHAHABQAHACALgBIAaAAIAlAAg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = rect = new cjs.Rectangle(-12,-7.7,24.1,15.5);
p.frameBounds = [rect];


(lib.Символ21 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgsBCQgIgCgDgHQgDgGADgPQAGgagBgQIgBgUIAAgOQABgFAEgHQADgGAGgEQAGgEAFABIAAgBQAbABAYADQAKABAHACQAKAFADAHIACALIgBANIgGAbQgFAVgDAJQgGAMgIAFQgEACgNABQgLABgEACIgMAFIgMAEIgIABIgIgBgAgNgWIgHABIABAoIAWgEIADAAIACgDQACgCgBgFIABgGIABgDIABgHIACgCIABgKIgUAAIgIABg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = rect = new cjs.Rectangle(-5.8,-6.7,11.6,13.5);
p.frameBounds = [rect];


(lib.Символ20 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("Ag3A+QgLgBgJgEQgFgDgIgJQgFgFgDgFQgFgLACgVIABgSQAAgNACgEQAEgJAOgEQAIgCAPAAIAXgFQAOgFAIgBIAcAAQATAAAKgEIABAAQAEABARgBQANAAAGAEQAJAFADAMQACAHgBAPQgDARgFAPQgFAPgGAGQgEADgIAEIgMAGIgRAHQgEACgQACQgiAFgYAAIgSgBgAA2gUQgFABgHAEIgnABIgKABIgIACQgSAHgVACQgJABgEABIAEAOIACAFQABADAFABQAFACAJgBIA6gGIAKgDIALgGIAIgCQAEgBACgDIACgFIADgHIABgFQAAgEACgCIgHAAg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = rect = new cjs.Rectangle(-10.1,-6.4,20.3,12.8);
p.frameBounds = [rect];


(lib.Символ19 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AheBTQgOgDgFgGQgGgHABgLQABgJAGgKQACgEAAgCQAAgDgEgEQgIgMAFgXQAEgPAGgHQAGgGAQgEQAUgHAMgCIAbgEIAZgFQAIgCAagKQAVgIANgCIABAAQAVABANAQQANAQgBAWIgCASQAAAKAGAZQACAagQAHQgHAEgMgDIgVgEQgKAAgPAGIgZAKIgXADIgrAIQgQACgMAAIgPgBgAA1ghIgPAHIgMAEIgIAFIgNACIgKADIgYACIgQADQgOAGgDABIgGAAQgCABAAAEQgCARACAUIAUgEIAlgHIAPgCQAJgBAJgFIAGgEQAGgDAFAAIASgDIASgDQgGgRAEgUQACgHgBgDIgEABIAAgBQgGABgJADg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = rect = new cjs.Rectangle(-11.9,-8.5,24,17);
p.frameBounds = [rect];


(lib.Символ18 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AAYBXIgYgIQgIgCgMgBIgVgBQgZgCgNgMQgJgIgCgLIgCgWIgEgRQgBgIABgHIAEgNQABgGAAgIIgCgPQgBgTALgHQAGgFALAAIAdgCIAAAAQATAAAbAHIAvALQARAEADACQAMAGAFAPQAEAJAAASQAAAVgHAWIgHAeQgDANgJAHQgHAHgOACIgOABIgMgBgAg0gsIAAAAQgBALACAMIAFANIADANQACAKgBAFIAAADIADABIAnAGQAFAAAHADIAIADIAIACQAHACABgBQABgBAAAAQABgBAAAAQAAgBAAAAQAAgBAAgBQACgKADgJIAFgPIAAgHIACgGQABgEgBgDIgOgEQgKgDgFgDIgGgDIgHgBIgIgDQgGgDgGABQABgBAAAAQAAgBAAAAQAAAAgBAAQAAAAgBAAIgTgBQABAAAAAAQAAgBgBAAQAAAAAAAAQgBgBAAAAIgLgBIgIABg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = rect = new cjs.Rectangle(-9.8,-8.9,19.6,17.8);
p.frameBounds = [rect];


(lib.Символ17 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AAVBRQgLgBgIgDIgOgFQgMgDgPgBIgeAAQgQAAgGgCQgNgDgEgKQgCgEAAgIIAAgOIgDgMQgDgPAGgeQAEgWAGgLQAKgSARgCIAAABQAPgCAWADIAkAFQAUACAXgCQAbgCALACQAWADAIAQQAEAJAAAQIAABLQAAAcgLAIQgIAFgRAAQgYAAgigDgAg3giQAAAQgCAHIgDATQgBAFgDAIIAiADQAWABAKAEIARAFQAIACAMAAIAgACIgBhFQg7AJg6gMIgHAAg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = rect = new cjs.Rectangle(-11.5,-8.4,23.2,16.9);
p.frameBounds = [rect];


(lib.Символ16 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("Ag7BZQgPAAgQgIQgJgFgEgGQgFgIAAgTQAAgJgCgFIgHgLQgEgGgCgKQAAgGABgKQACgVAFgKQAFgJAGgHQAJgHAIgCIAZgEIAZgEQAOgCAZgBQAYgBAvgGIAAAAQATgBAIACQAPADAFAMQADAFABAMQABAigGAjQgFAdgLANQgTAXgqACQgrgBgXAEIgRADQgJACgHAAIgCAAgABMgoIg/AGIgYACIgVABIgMADIgZAAIgFABQgCACgBADQgEANAEALIAHAQIABALIABALIADAFIAGABQAGAAANgEQAQgEAiAAQAiABASgGIAHgEQACgCAAgFIABgHQADgKABgFIAAgMIACgPQABgJgBgFIgDAAg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = rect = new cjs.Rectangle(-12.4,-9,24.9,18);
p.frameBounds = [rect];


(lib.Символ15 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AhHBVQgJgJgFgPIgHgcQgJgVgDgLQgEgSAIgNQAHgKAWgIIB+gtIABABQAMgBAJABQAMACAGAIQAEAGACAOIAEAwIABAVQAAALgEAHQgDAHgIAIQgVAVgPAGIgTAEQgkAFgIACQgOADgMAHIgKAFIgHABQgKAAgKgJgAA6guIhSAhIgRAEQgKADgGAEIAPAvQAJgBAKgDIAMgEIASgDIAVgDQANgCAHgEIALgLIAGgHIABgHIgBgJIgCgHIAAgNQAAgEgDgNIgCAAg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = rect = new cjs.Rectangle(-10.6,-9.4,21.3,18.9);
p.frameBounds = [rect];


(lib.Символ47 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Символ 29
	this.instance = new lib.Символ29();
	this.instance.setTransform(280.8,7.6,1,1,15);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({rotation:0,guide:{path:[280.7,7.5,187.1,-19.6,82,3.8,-15,25.1,-103.8,-0.1,-173.6,-20.1,-243.8,-4.7], orient:'auto'}},399).to({scaleX:1,scaleY:1,rotation:12.3,guide:{path:[-243.8,-4.8,-263.1,-0.6,-282.4,6.1,-292.6,8.8,-303.3,8.4,-314,8,-332.4,5.1,-342.1,3.6,-365.6,1.8], orient:'auto'}},100).to({rotation:-2.3,guide:{path:[-365.6,1.5,-386.9,-0.3,-419.4,-2.4,-460,-5.2,-494,-2.5], orient:'auto'}},100).to({scaleX:1,scaleY:1,rotation:-0.5,guide:{path:[-494,-2.4,-517.4,-0.6,-537.8,4,-592,15.7,-643.7,13.1], orient:'auto'}},100).to({rotation:0,guide:{path:[-643.7,13,-684.6,10.9,-723.8,-0.1,-743.1,-5.6,-762.6,-8.5], orient:'auto'}},100).wait(1));

	// Символ 28
	this.instance_1 = new lib.Символ28();
	this.instance_1.setTransform(306.8,16.1,1,1,15);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).to({rotation:0,guide:{path:[306.8,16,304.6,15.2,302.5,14.5,199.5,-22.4,82,3.7,-14.9,25,-103.8,-0.2,-158.5,-15.8,-213.4,-9.8], orient:'auto'}},399).to({rotation:15,guide:{path:[-213.4,-9.7,-247.9,-6,-282.5,6.1,-292.7,8.8,-303.4,8.4,-314.1,8,-332.5,5.1,-333.6,4.9,-334.9,4.7], orient:'auto'}},100).to({scaleX:1,scaleY:1,rotation:10.1,guide:{path:[-334.9,4.7,-355,1.9,-419.3,-2.4,-441.9,-4,-462.5,-3.8], orient:'auto'}},100).to({scaleX:1,scaleY:1,rotation:4.3,guide:{path:[-462.4,-3.8,-504.3,-3.5,-537.8,4,-575.3,12,-611.7,13.3], orient:'auto'}},100).to({rotation:0,x:-732.7,y:-10.2},100).wait(1));

	// Символ 27
	this.instance_2 = new lib.Символ27();
	this.instance_2.setTransform(343.8,24.7);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({guide:{path:[343.8,24.6,318.1,20.2,302.5,14.6,199.5,-22.3,82.2,4,-14.9,25.1,-103.8,-0.1,-140.2,-10.5,-176.8,-11.3], orient:'auto'}},399).to({rotation:-15,guide:{path:[-176.8,-11.3,-229.5,-12.5,-282.5,6.1,-288.9,7.8,-295.6,8.3], orient:'auto'}},100).to({scaleX:1,scaleY:1,rotation:-10.1,guide:{path:[-295.6,8.3,-299.4,8.5,-303.3,8.4,-314,7.9,-332.4,5.1,-350.8,2.4,-419.3,-2.4,-421.6,-2.6,-423.8,-2.7], orient:'auto'}},100).to({scaleX:1,scaleY:1,rotation:-4.3,guide:{path:[-423.8,-2.7,-489.5,-6.8,-537.8,3.8,-555.2,7.6,-572.4,9.9], orient:'auto'}},100).to({rotation:0,x:-696,y:-13},100).wait(1));

	// Символ 26
	this.instance_3 = new lib.Символ26();
	this.instance_3.setTransform(383.8,30.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).to({guide:{path:[383.6,30.1,328.6,24,302.5,14.6,199.4,-22.3,82.2,4,-15,25.1,-103.9,-0.1,-121.1,-5,-138.3,-7.8], orient:'auto'}},399).to({regX:-0.1,regY:-0.1,rotation:-30,guide:{path:[-138.3,-7.8,-198.1,-17.4,-258.3,-1.4], orient:'auto'}},100).to({scaleX:1,scaleY:1,rotation:-20.3,guide:{path:[-258.3,-1.2,-270.4,2.1,-282.4,6.1,-292.7,8.9,-303.3,8.4,-314,8,-332.4,5.1,-345.6,3.3,-384.8,0.2], orient:'auto'}},100).to({rotation:-9,guide:{path:[-384.8,0,-400.1,-1,-419.3,-2.4,-485.8,-7,-534.8,3.1], orient:'auto'}},100).to({regX:0,regY:0,scaleX:1,scaleY:1,rotation:0,x:-657.6,y:-7.2},100).wait(1));

	// Символ 25
	this.instance_4 = new lib.Символ25();
	this.instance_4.setTransform(422.6,33.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).to({guide:{path:[422.5,33.6,337.1,27.1,302.5,14.6,199.4,-22.3,82.2,4,-13.2,24.7,-100.7,0.8], orient:'auto'}},399).to({rotation:-15,guide:{path:[-100.9,0.5,-102.4,0.1,-104,-0.4,-162.7,-17,-222,-8.7], orient:'auto'}},100).to({scaleX:1,scaleY:1,rotation:-10.1,guide:{path:[-222.1,-8.6,-252.2,-4.4,-282.5,6.1,-292.7,8.9,-303.4,8.4,-314,8,-332.4,5.1,-338.3,4.2,-349.2,3.1], orient:'auto'}},100).to({scaleX:1,scaleY:1,rotation:-4.3,guide:{path:[-349.2,3.1,-372.7,0.7,-419.4,-2.4,-463.7,-5.5,-500.2,-1.9], orient:'auto'}},100).to({rotation:0,x:-620.2,y:1.6},100).wait(1));

	// Символ 24
	this.instance_5 = new lib.Символ24();
	this.instance_5.setTransform(456.5,36);

	this.timeline.addTween(cjs.Tween.get(this.instance_5).to({guide:{path:[456.4,35.9,343.6,29.4,302.5,14.5,199.4,-22.4,82.2,3.9,4.6,20.8,-67.6,8.2], orient:'auto'}},399).to({guide:{path:[-67.8,8.1,-86.1,4.8,-103.9,-0.3,-146.2,-12.2,-188.6,-11.3], orient:'auto'}},100).to({guide:{path:[-188.5,-11.4,-235.4,-10.4,-282.4,6.1,-292.6,8.8,-303.3,8.4,-308.9,8.2,-316.6,7.3], orient:'auto'}},100).to({guide:{path:[-316.6,7.3,-323.6,6.5,-332.4,5.1,-350.8,2.3,-419.3,-2.3,-444.1,-4.1,-466.5,-3.7], orient:'auto'}},100).to({x:-586.5,y:5.9},100).wait(1));

	// Символ 23
	this.instance_6 = new lib.Символ23();
	this.instance_6.setTransform(490.1,36.3);

	this.timeline.addTween(cjs.Tween.get(this.instance_6).to({guide:{path:[490,37.3,480,37.2,470.6,36.7,346.2,30.3,302.5,14.8,199.7,-22.3,82.2,4,22.1,17,-34.9,12.5], orient:'auto'}},399).to({scaleX:1,scaleY:1,rotation:4.6,guide:{path:[-35.1,12.2,-70.2,9.3,-103.8,-0.2,-128.8,-7.3,-153.8,-9.9], orient:'auto'}},100).to({rotation:-16.8,guide:{path:[-153.8,-9.9,-217.1,-16.5,-280.9,5.5], orient:'auto'}},100).to({rotation:9.3,guide:{path:[-280.9,5.5,-281.7,5.8,-282.5,6.1,-292.7,8.8,-303.4,8.4,-314.1,7.9,-332.4,5.1,-350.8,2.4,-419.4,-2.4,-427.3,-3,-435,-3.3], orient:'auto'}},100).to({scaleX:1,scaleY:1,rotation:0,guide:{path:[-435,-3.3,-493.7,-5.9,-537.8,3.8,-546.2,5.6,-554.5,7.1], orient:'auto'}},100).wait(1));

	// Символ 22
	this.instance_7 = new lib.Символ22();
	this.instance_7.setTransform(524.1,36.6);

	this.timeline.addTween(cjs.Tween.get(this.instance_7).to({guide:{path:[524,36.6,495.1,37.9,470.5,36.7,346.2,30.3,302.5,14.7,199.6,-22.4,82.2,3.9,39.5,13.3,-1.5,13.7], orient:'auto'}},399).to({scaleX:1,scaleY:1,rotation:12,guide:{path:[-1.7,13.4,-54.1,13.9,-103.8,-0.2,-112.5,-2.6,-121.2,-4.6], orient:'auto'}},100).to({scaleX:1,scaleY:1,rotation:-15,guide:{path:[-121.4,-4.7,-185.1,-18.7,-249.4,-3.7], orient:'auto'}},100).to({rotation:-6.5,guide:{path:[-249.4,-3.5,-265.9,0.4,-282.5,6.1,-292.7,8.8,-303.4,8.4,-314.1,7.9,-332.5,5.1,-348,2.7,-399.2,-0.9], orient:'auto'}},100).to({rotation:0,x:-520.7,y:13.5},100).wait(1));

	// Символ 21
	this.instance_8 = new lib.Символ21();
	this.instance_8.setTransform(552.7,34.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_8).to({guide:{path:[552.6,34.7,506.9,38.5,470.5,36.7,346.2,30.3,302.5,14.7,199.6,-22.4,82.2,3.9,54.1,10.1,26.7,12.4], orient:'auto'}},399).to({scaleX:1,scaleY:1,rotation:21.1,guide:{path:[26.6,12.2,-36.2,17.4,-95.6,2.1], orient:'auto'}},100).to({scaleX:1,scaleY:1,rotation:0,guide:{path:[-95.9,1.9,-100,0.8,-104.1,-0.3,-164.4,-17.4,-225.4,-8.2], orient:'auto'}},100).to({guide:{path:[-225.4,-8.2,-253.9,-3.8,-282.5,6.1,-292.7,8.8,-303.4,8.4,-314.1,8,-332.5,5.1,-343.8,3.4,-374.2,0.8], orient:'auto'}},100).to({x:-492.5,y:11.9},100).wait(1));

	// Символ 20
	this.instance_9 = new lib.Символ20();
	this.instance_9.setTransform(574,32.7);

	this.timeline.addTween(cjs.Tween.get(this.instance_9).to({guide:{path:[573.9,32.6,515.3,39,470.5,36.7,346.2,30.3,302.5,14.7,199.6,-22.4,82.2,3.9,64.8,7.8,47.6,10.1], orient:'auto'}},399).to({scaleX:1,scaleY:1,rotation:16.8,guide:{path:[47.4,9.9,-14.9,18.3,-73.9,7], orient:'auto'}},100).to({scaleX:1,scaleY:1,rotation:0,guide:{path:[-74.1,6.9,-89.2,3.9,-103.9,-0.3,-152.7,-14.1,-201.8,-10.8], orient:'auto'}},100).to({guide:{path:[-201.8,-10.8,-242,-8,-282.5,6.1,-292.7,8.8,-303.4,8.4,-314.1,8,-332.5,5.1,-338.3,4.2,-349.2,3.1], orient:'auto'}},100).to({x:-471.9,y:7.9},100).wait(1));

	// Символ 19
	this.instance_10 = new lib.Символ19();
	this.instance_10.setTransform(609.1,28.3);

	this.timeline.addTween(cjs.Tween.get(this.instance_10).to({guide:{path:[609.1,28.2,528.2,39.6,470.5,36.7,346.2,30.3,302.7,14.7,199.7,-22.4,82.2,3.9], orient:'auto'}},399).to({scaleX:1,scaleY:1,rotation:17,guide:{path:[82,3.9,82,3.9,82,3.9,20.3,17.3,-38.2,11.9]}},100).to({scaleX:1,scaleY:1,rotation:15,guide:{path:[-38.5,11.9,-71.8,8.9,-103.9,-0.1,-134.5,-8.9,-165.2,-10.8], orient:'auto'}},100).to({rotation:6.5,guide:{path:[-165.2,-10.8,-223.6,-14.6,-282.5,6.1,-292.7,8.9,-303.4,8.4,-308.7,8.2,-316,7.4], orient:'auto'}},100).to({rotation:0,x:-437,y:3.9},100).wait(1));

	// Символ 15
	this.instance_11 = new lib.Символ15();
	this.instance_11.setTransform(647.9,22.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_11).to({guide:{path:[647.8,22.1,541.6,40.3,470.6,36.7,346.2,30.3,302.7,14.7,216.6,-16.3,120.4,-3], orient:'auto'}},399).to({scaleX:1,scaleY:1,rotation:12.3,guide:{path:[120.2,-3,101.3,-0.4,82,4,40.5,12.9,0.2,13.5], orient:'auto'}},100).to({scaleX:1,scaleY:1,rotation:30,guide:{path:[0,13.5,-53.4,14.2,-103.8,-0.1,-115.6,-3.5,-127.4,-5.8], orient:'auto'}},100).to({regX:0.1,regY:-0.1,scaleX:1,scaleY:1,rotation:13.3,guide:{path:[-127.6,-5.8,-201.7,-20.7,-276.5,4.1], orient:'auto'}},100).to({regX:0,regY:0,scaleX:1,scaleY:1,rotation:0,guide:{path:[-276.5,4,-279.5,5,-282.5,6.1,-292.7,8.8,-303.4,8.4,-314.1,7.9,-332.5,5.1,-347.9,2.9,-399,-0.9], orient:'auto'}},100).wait(1));

	// Символ 16
	this.instance_12 = new lib.Символ16();
	this.instance_12.setTransform(689.5,14.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_12).to({guide:{path:[689.4,14.6,555.1,41,470.6,36.7,346.2,30.3,302.7,14.7,235.6,-9.4,162.4,-6.7], orient:'auto'}},399).to({rotation:-4.8,guide:{path:[162.2,-6.7,122.9,-5.2,82,3.8,62.1,8.2,42.3,10.6], orient:'auto'}},100).to({rotation:15,guide:{path:[42.1,10.6,-23.7,18.6,-85.8,4.5], orient:'auto'}},100).to({rotation:-8.5,guide:{path:[-86,4.3,-95.1,2.2,-104.1,-0.3,-170.1,-19.1,-236.7,-6.2], orient:'auto'}},100).to({rotation:15,guide:{path:[-236.7,-6.2,-259.5,-1.8,-282.4,6.1,-292.7,8.9,-303.3,8.4,-314,8,-332.4,5.1,-340.2,3.9,-357,2.4], orient:'auto'}},100).wait(1));

	// Символ 17
	this.instance_13 = new lib.Символ17();
	this.instance_13.setTransform(730,6);

	this.timeline.addTween(cjs.Tween.get(this.instance_13).to({guide:{path:[730,6.1,567.6,41.6,470.6,36.7,346.4,30.3,302.7,14.7,255.3,-2.3,204.8,-6], orient:'auto'}},399).to({scaleX:1,scaleY:1,rotation:-17.8,guide:{path:[204.5,-6,145.6,-10.2,82.7,3.6], orient:'auto'}},100).to({scaleX:1,scaleY:1,rotation:0,guide:{path:[82.5,3.8,82.2,3.9,82,3.9,16,18.3,-46.2,11.1], orient:'auto'}},100).to({guide:{path:[-46.4,11.1,-75.7,7.8,-103.9,-0.2,-150.2,-13.4,-196.8,-11.1], orient:'auto'}},100).to({guide:{path:[-196.7,-11.1,-239.5,-9,-282.4,6.1,-292.6,8.8,-303.3,8.4,-308.3,8.1,-314.8,7.4], orient:'auto'}},100).wait(1));

	// Символ 18
	this.instance_14 = new lib.Символ18();
	this.instance_14.setTransform(765.5,-2.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_14).to({guide:{path:[765.4,-2,577.9,42.2,470.5,36.7,346.4,30.3,302.7,14.8,273.6,4.3,243.3,-1.2], orient:'auto'}},399).to({scaleX:1,scaleY:1,rotation:-15.8,guide:{path:[243.1,-1.4,184.4,-11.7,121.6,-3.3], orient:'auto'}},100).to({rotation:-5.5,guide:{path:[121.4,-3.2,101.9,-0.5,82,3.9,37.1,13.6,-6.2,13.5], orient:'auto'}},100).to({rotation:2.3,guide:{path:[-6.4,13.5,-56.4,13.3,-103.9,-0.2,-130,-7.6,-156.2,-10.2], orient:'auto'}},100).to({scaleX:1,scaleY:1,rotation:-15,guide:{path:[-156.1,-10.1,-215.8,-15.9,-275.9,3.9], orient:'auto'}},100).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = rect = new cjs.Rectangle(271,-10.9,504.8,55.7);
p.frameBounds = [rect, rect=null, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, new cjs.Rectangle(-252.2,-20.1,505,40.9), rect=null, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, new cjs.Rectangle(-375.7,-20.3,509.5,45.8), rect=null, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, new cjs.Rectangle(-502.5,-22.3,507.1,46.1), rect=null, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, new cjs.Rectangle(-652.3,-19.4,506.4,40.8), rect=null, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, new cjs.Rectangle(-771.2,-21.1,507,41.9)];


(lib.Символ46 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Символ 44
	this.instance = new lib.Символ44();
	this.instance.setTransform(763.6,-15);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({guide:{path:[763.5,-14.9,575.1,17,418.7,-5.4,362.1,-14.9,330.2,-11.5,298.2,-8,266.6,-1.4,253.4,1.5,241.7,3.6], orient:'auto'}},399).to({rotation:30,guide:{path:[241.5,3.6,225.2,6.5,212,8.1,189.2,10.7,169.8,10.3,83.1,8.2,-4.1,6.1,-28.2,5.2,-87.8,4.2,-134.9,3.4,-159.6,1.8,-195.6,-0.4,-222.8,-5.6,-236.2,-8.2,-252.8,-12.9,-254.1,-13.2,-276.3,-19.9], orient:'auto'}},400).wait(1));

	// Символ 43
	this.instance_1 = new lib.Символ43();
	this.instance_1.setTransform(725.7,-9.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).to({guide:{path:[725.7,-9,559.3,14.7,418.7,-5.2,362.2,-14.9,330.2,-11.5,298.3,-8,266.6,-1.4,235,5.4,212.2,8.1,207.4,8.7,202.6,9.1], orient:'auto'}},399).to({rotation:15,guide:{path:[202.4,9.1,185.1,10.7,169.9,10.3,83.2,8.2,-4,6.1,-28.4,5.3,-87.7,4.2,-134.8,3.4,-159.6,1.9,-195.6,-0.3,-222.8,-5.7,-236.2,-8.2,-252.8,-12.9,-254.1,-13.2,-280.8,-21.2,-290.2,-24,-314.9,-26.7], orient:'auto'}},400).wait(1));

	// Символ 42
	this.instance_2 = new lib.Символ42();
	this.instance_2.setTransform(685.2,-3.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({guide:{path:[685.1,-3.8,542,12.2,418.7,-5.2,361.9,-15,330.2,-11.5,298.2,-8.1,266.6,-1.2,235,5.4,212.2,8.1,189.4,10.7,170,10.3,165.4,10.1,160.7,10], orient:'auto'}},399).to({guide:{path:[160.6,10.1,78.5,8.1,-4,6.1,-28.4,5.3,-87.9,4.2,-134.8,3.4,-159.6,1.9,-195.6,-0.3,-222.8,-5.7,-236.2,-8.4,-252.8,-12.9,-254.1,-13.2,-280.8,-21.2,-294,-25.2,-337.5,-28.8,-346.7,-29.6,-356.3,-30.2], orient:'auto'}},400).wait(1));

	// Символ 41
	this.instance_3 = new lib.Символ41();
	this.instance_3.setTransform(650.3,-0.4);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).to({guide:{path:[650.3,-0.5,526.9,10,418.7,-5.2,362,-15,330,-11.5,298.3,-8.1,266.7,-1.2,235,5.4,212.3,8.1,189.5,10.7,170.1,10.3,147.7,9.7,125.2,9.2], orient:'auto'}},399).to({guide:{path:[125,9.2,60.7,7.7,-4,6.1,-28.4,5.3,-87.9,4.2,-134.8,3.4,-159.6,1.9,-195.6,-0.3,-222.8,-5.7,-236.2,-8.4,-252.8,-12.9,-254.1,-13.2,-280.8,-21.2,-294,-25.2,-337.5,-28.8,-363.7,-31,-391.9,-32.1], orient:'auto'}},400).wait(1));

	// Символ 40
	this.instance_4 = new lib.Символ40();
	this.instance_4.setTransform(618.4,1.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).to({guide:{path:[618.3,1.8,512.7,8,418.7,-5.2,361.9,-15,330,-11.5,298.3,-8.1,266.6,-1.2,235,5.4,212.2,8.1,189.5,10.7,170.1,10.3,131.5,9.3,92.9,8.4], orient:'auto'}},399).to({guide:{path:[92.7,8.4,44.4,7.3,-4,6.1,-28.4,5.2,-87.9,4.2,-135,3.4,-159.8,1.8,-195.6,-0.4,-222.8,-5.8,-236.2,-8.4,-252.8,-12.9,-254.1,-13.3,-280.8,-21.2,-294,-25.2,-337.5,-28.8,-378.4,-32.2,-424,-33.1], orient:'auto'}},400).wait(1));

	// Символ 39
	this.instance_5 = new lib.Символ39();
	this.instance_5.setTransform(587.1,3.3);

	this.timeline.addTween(cjs.Tween.get(this.instance_5).to({guide:{path:[587,3.2,498.6,6,418.7,-5.2,361.9,-15,330,-11.5,298,-8.1,266.6,-1.2,235,5.4,212.2,8.1,189.4,10.7,170,10.3,115.5,8.9,60.8,7.6], orient:'auto'}},399).to({guide:{path:[60.6,7.7,28.3,6.9,-4.1,6.1,-28.4,5.3,-88,4.2,-135.1,3.4,-159.8,1.9,-195.8,-0.3,-222.8,-5.7,-236.2,-8.4,-252.8,-12.9,-254.2,-13.2,-280.8,-21.2,-294.1,-25.2,-337.5,-28.8,-385.8,-32.8,-440.6,-33.3,-448.5,-33.4,-456.5,-33.4], orient:'auto'}},400).wait(1));

	// Символ 38
	this.instance_6 = new lib.Символ38();
	this.instance_6.setTransform(555.3,3.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_6).to({guide:{path:[555.3,3.8,484.2,3.9,418.7,-5.2,362,-15,330,-11.5,298.1,-8.1,266.7,-1.2,235,5.4,212.3,8.1,189.5,10.7,170.1,10.3,100.3,8.6,30,6.9], orient:'auto'}},399).to({guide:{path:[29.8,6.9,12.9,6.5,-4,6.1,-28.4,5.2,-87.9,4.2,-135,3.4,-159.8,1.8,-195.8,-0.4,-223,-5.8,-236.2,-8.4,-252.8,-13.1,-254.1,-13.3,-280.8,-21.2,-294,-25.2,-337.5,-28.8,-385.8,-32.8,-440.6,-33.4,-463.1,-33.6,-487.3,-32.7], orient:'auto'}},400).wait(1));

	// Символ 37
	this.instance_7 = new lib.Символ37();
	this.instance_7.setTransform(519.3,3.3,1,1,-15);

	this.timeline.addTween(cjs.Tween.get(this.instance_7).to({guide:{path:[519.2,3.2,467.4,1.5,418.7,-5.2,362,-15,330,-11.5,298.1,-8.1,266.4,-1.2,235,5.4,212.2,8.1,189.5,10.7,170.1,10.3,83.4,8.2,-3.8,6.1,-3.8,6.1,-3.8,6.1]}},399).to({rotation:0,guide:{path:[-4,6.1,-28.4,5.2,-87.9,4.2,-135,3.4,-159.8,1.8,-195.8,-0.4,-223,-5.8,-236.4,-8.4,-253,-13.1,-254.3,-13.5,-280.8,-21.2,-294,-25.2,-337.5,-28.8,-385.8,-32.8,-440.6,-33.4,-478.4,-33.7,-520.8,-30.9], orient:'auto'}},400).wait(1));

	// Символ 36
	this.instance_8 = new lib.Символ36();
	this.instance_8.setTransform(485.3,1.6);

	this.timeline.addTween(cjs.Tween.get(this.instance_8).to({guide:{path:[485.3,1.5,451.3,-0.8,418.7,-5.2,362,-15,330,-11.5,298.1,-8.1,266.4,-1.2,234.8,5.4,212.2,8.1,189.5,10.7,170.1,10.3,83.4,8.2,-3.8,6.1,-15.7,5.6,-36,5.2], orient:'auto'}},399).to({guide:{path:[-36.2,5.3,-57.4,4.8,-87.9,4.2,-135,3.4,-159.8,1.9,-195.8,-0.3,-223,-5.7,-236.4,-8.4,-253,-13.1,-254.3,-13.4,-280.8,-21.2,-294,-25.2,-337.5,-28.8,-385.8,-32.8,-440.6,-33.3,-492.5,-33.9,-553,-28.3], orient:'auto'}},400).wait(1));

	// Символ 35
	this.instance_9 = new lib.Символ35();
	this.instance_9.setTransform(449.3,-1.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_9).to({guide:{path:[449.2,-1.4,433.8,-3.1,418.7,-5.2,361.9,-15,330,-11.5,298,-8.1,266.4,-1.2,234.8,5.4,212,8.1,189.2,10.7,170,10.3,83.4,8.2,-3.9,6.1,-24.8,5.3,-71.7,4.4], orient:'auto'}},399).to({guide:{path:[-71.9,4.5,-79.6,4.4,-88,4.2,-135.1,3.4,-159.8,1.9,-195.8,-0.3,-223,-5.7,-236.4,-8.4,-253,-13.1,-254.4,-13.4,-281,-21.2,-294.3,-25.2,-337.5,-28.8,-385.8,-32.8,-440.6,-33.3,-507.3,-34,-588.4,-24.7], orient:'auto'}},400).wait(1));

	// Символ 34
	this.instance_10 = new lib.Символ34();
	this.instance_10.setTransform(413.3,-6.3);

	this.timeline.addTween(cjs.Tween.get(this.instance_10).to({guide:{path:[413.2,-6.2,360.4,-14.8,330,-11.5,298.1,-8.1,266.4,-1.2,234.8,5.4,212,8.1,189.3,10.7,169.9,10.3,83.4,8.2,-3.8,6.1,-28.2,5.2,-87.7,4.2,-98.6,4,-108.3,3.7], orient:'auto'}},399).to({guide:{path:[-108.6,3.7,-140.8,3,-159.8,1.8,-195.8,-0.4,-223,-5.8,-236.4,-8.4,-253,-13.1,-254.4,-13.5,-281,-21.2,-294.3,-25.2,-337.5,-28.8,-385.8,-32.8,-440.6,-33.4,-521.9,-34.2,-624.3,-20.3], orient:'auto'}},400).wait(1));

	// Символ 33
	this.instance_11 = new lib.Символ33();
	this.instance_11.setTransform(381.6,-10.6);

	this.timeline.addTween(cjs.Tween.get(this.instance_11).to({guide:{path:[381.5,-10.5,350.6,-13.7,330,-11.5,298,-8.1,266.4,-1.2,234.8,5.4,212,8.1,189.2,10.7,169.8,10.3,83.4,8.2,-3.9,6.1,-28.2,5.2,-87.8,4.2,-118.7,3.6,-140.1,2.8], orient:'auto'}},399).to({guide:{path:[-140.2,2.9,-151.3,2.4,-159.8,1.9,-195.8,-0.3,-223,-5.7,-236.4,-8.4,-253,-13.1,-254.4,-13.4,-281,-21.2,-294.3,-25.2,-337.5,-28.8,-385.8,-32.8,-440.6,-33.3,-534,-34.3,-655.4,-15.7], orient:'auto'}},400).wait(1));

	// Символ 32
	this.instance_12 = new lib.Символ32();
	this.instance_12.setTransform(349.5,-12.3);

	this.timeline.addTween(cjs.Tween.get(this.instance_12).to({guide:{path:[349.4,-12.4,338.8,-12.5,330,-11.5,298.1,-8.1,266.4,-1.2,234.8,5.4,212,8.1,189.3,10.7,169.9,10.3,83.4,8.2,-3.8,6.1,-28.2,5.2,-87.7,4.2,-134.8,3.4,-159.6,1.8,-166.2,1.4,-172.5,0.9], orient:'auto'}},399).to({guide:{path:[-172.6,0.9,-200.7,-1.4,-223,-5.8,-236.4,-8.4,-253,-13.1,-254.3,-13.5,-281,-21.2,-294.2,-25.2,-337.7,-28.8,-385.8,-32.8,-440.6,-33.4,-546,-34.5,-687.2,-10.8], orient:'auto'}},400).wait(1));

	// Символ 31
	this.instance_13 = new lib.Символ31();
	this.instance_13.setTransform(312.4,-9.4);

	this.timeline.addTween(cjs.Tween.get(this.instance_13).to({guide:{path:[312.3,-9.3,289.3,-6.1,266.4,-1.2,234.7,5.5,212,8.1,189.2,10.8,169.8,10.3,83.2,8.2,-3.9,6.1,-28.2,5.3,-87.8,4.2,-134.9,3.4,-159.6,1.9,-187.3,0.4,-209.8,-3.2], orient:'auto'}},399).to({guide:{path:[-209.9,-3.4,-216.7,-4.5,-223,-5.7,-236.4,-8.4,-253,-13.1,-254.3,-13.4,-281,-21.2,-294.2,-25.2,-337.7,-28.8,-385.8,-32.8,-440.6,-33.3,-559.4,-34.6,-723.6,-4.3], orient:'auto'}},400).wait(1));

	// Символ 30
	this.instance_14 = new lib.Символ30();
	this.instance_14.setTransform(278.7,-3.9,1,1,-30);

	this.timeline.addTween(cjs.Tween.get(this.instance_14).to({rotation:0,guide:{path:[278.6,-3.7,272.5,-2.5,266.4,-1.2,234.8,5.4,212,8.1,189.2,10.8,169.8,10.3,83.2,8.2,-3.9,6.1,-28.2,5.3,-87.8,4.2,-134.9,3.4,-159.6,1.9,-195.6,-0.1,-222.8,-5.5,-232.2,-7.4,-243.3,-10.3], orient:'auto'}},399).to({guide:{path:[-243.5,-10.5,-248.1,-11.7,-253,-13.1,-254.4,-13.5,-281,-21.2,-294.3,-25.2,-337.7,-28.8,-385.8,-32.8,-440.6,-33.4,-571.3,-34.7,-756.9,2.1], orient:'auto'}},400).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = rect = new cjs.Rectangle(267.4,-23.7,507.1,37.1);
p.frameBounds = [rect, rect=null, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, new cjs.Rectangle(-251.6,-18.2,504.1,36.3), rect=null, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, new cjs.Rectangle(-765.6,-40.2,503.1,50.6)];


(lib.Символ14 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 2
	this.instance = new lib.fotokonkurs();
	this.instance.setTransform(-159.7,-35.6);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(400));

	// Слой 3 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AY4K8QgHgCgKAAIgRgBIgdgFQgRgEgLACQgGABgLAEQgLAFgFABQgMADgUgBQgZgCgogFIhBgHQgbgChRgCQgogCggABIgvAAIiUgMIgBAAQgSgDgIADQgEACgDADIpWgwIAYADIh0AEIh4AFQhEACgygGIhNgJQgUgCiKgHQhKgEg2gJQgmgBgHACIgIgBQgigCgbAAIAAgBQhWAGikgDQirgEhQAFIg4AEQggABgRgGQgSgHgQAFQgQAEgEgCQgFgCgOABQhYAFhugFQgxgBhfgGIgFgFIgJgBQgYAAgZgDIgBABQgDgBgCACIgDACIhCgFIgFgCIgKgGIgGgBIgHgDQgGgBgHABIgDABIgKABIgPAEIgBAAIAAAAIgBgBQgEgEgEgCQgQgIgOACIgKAAIgMACIgIAFQhtgMhXgSQhdgWgvgIIhTgLQg0gHgggHQhAgOhNggQgrgShSgoQABgFAAgGQAAgKgEgHIgBgCIAAgBQAEgQABgdIAEhaQACgsAAgWIgCghQgBgUABgNIADgiIAEghQABgPgCgSIgCgkQAAgSACg3IAKjGQACgXgDgOIgBgIIABgFIACgTIAAgJIgCgOIABgSIgDgWIgGg0QgCgLgCgDQgDgFgEgBQgCgGgFgHQgNgQgXgSIgpgeIgUgSQAQABAWgCQAggCAogGIBGgNQFPg+EdgCQEdgCB7AeQB6AdAUADQAUADAoANQBcAhBiAcQAAAFACADQACACAGABQAIACAEgCIADgDIAAAAQCfAsCuAeQEwA1E1gEIBLgFIANAAIDAAAIBSgCQAugCAkgFQAmgGBMgSIBfgNQA8gIBKgSQBKgTgpAIIgOADIBAgRQBfgcAggHQAbgFA3gIQAVgECCgfQBTgTA6gEIAKADIALABIAIgBQAEgCACgDIACgDIABAAIAFgDQAVAAAhgFQCUgSCrAEQCNADCzATIAOABIA3AGQBHAHCIAdQCKAdBFAHIA8AIQARAEAHAHIAGALIAAAFQAGB3gJCDQgFA2gBAbQgBAhAFBCQAJB9ALA+IAJA3QAFAhABARQABAQAFAXIARBWIAIAaQALAmAMAyIAHAgIAHAqQADAZAFAQIgKAGQgVAMg2AcIgeAOQgSAGggAHQglAIgNAEQgZAJgLABIgXABQgPAAgJACQgIACgFAEIgfACQgcADgTAMIgOAHQgIADgGgDQgDgBgEgEIgGgGQgMgIgYAHQgrAMgWADQgXAEgfABQhHAFgagBIg1gBQgfAAgWAFIgrALQgTAEgVABQgWABgmgDQgsgEgQAAQgrgCgsAKIgJgEg");
	mask.setTransform(86.8,4.2);

	// Слой 6
	this.instance_1 = new lib.Символ47("synched",399);
	this.instance_1.setTransform(83.7,-39.3);

	this.instance_1.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(400));

	// Слой 4
	this.instance_2 = new lib.Символ47("synched",0);
	this.instance_2.setTransform(83.7,-39.3);

	this.instance_2.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(400));

	// Слой 8
	this.instance_3 = new lib.Символ46("synched",399);
	this.instance_3.setTransform(90.1,50.7);

	this.instance_3.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(400));

	// Слой 7
	this.instance_4 = new lib.Символ46("synched",0);
	this.instance_4.setTransform(90.1,50.7);

	this.instance_4.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(400));

	// Слой 1
	this.instance_5 = new lib.Символ45();

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(400));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = rect = new cjs.Rectangle(-358.5,-78.8,717.1,157.6);
p.frameBounds = [rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect];


(lib.Символ50 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.instance = new lib.Символ14();
	this.instance.setTransform(0,0,1,1,0,0,0,-373,-89);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = rect = new cjs.Rectangle(14.5,10.2,1223,157.7);
p.frameBounds = [rect];


// stage content:
(lib.fotokonkurs_Canvas = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		var r = this;
		var w = stage4.canvas.width;
		var h = stage4.canvas.height;
		
		if(w>4*h){
			
			r.clip.scaleX = h/180;
			r.clip.scaleY = h/180;
			
				if(r.clip.scaleY>=1){
					r.clip.scaleX = 1;
					r.clip.scaleY = 1;			
				}
			
		
		}else{
			
			r.clip.scaleX = w/750;
			r.clip.scaleY = w/750;	
			
				if(r.clip.scaleX>=1){
					r.clip.scaleX = 1;
					r.clip.scaleY = 1;			
				}	
			
		}
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// Слой 6
	this.clip = new lib.Символ50();

	this.timeline.addTween(cjs.Tween.get(this.clip).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = rect = new cjs.Rectangle(389.5,100.2,1223,157.7);
p.frameBounds = [rect];

})(lib4 = lib4||{}, images4 = images4||{}, createjs = createjs||{}, ss = ss||{});
var lib4, images4, createjs, ss;